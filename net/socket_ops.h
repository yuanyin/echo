#ifndef SOCKET_OPS_H_
#define SOCKET_OPS_H_

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

namespace sock {

///
/// Creates a non-blocking socket file descriptor,
/// abort if any error.
int create_sockfd_nonblocking(sa_family_t family);

int connect(int sockfd, const struct sockaddr* addr);
void bind_or_abort(int sockfd, const struct sockaddr* addr);
void listen_or_abort(int sockfd);
int accept_or_abort(int sockfd, struct sockaddr_in6* addr);
int create_bind_listen(int port);

ssize_t read(int sockfd, void* buf, size_t count);
ssize_t readv(int sockfd, const struct iovec* iov, int iovcnt);
ssize_t write(int sockfd, const void* buf, size_t count);
void close(int sockfd);
void shutdown_with_write(int sockfd);

void to_ip_port(char* buf, size_t size, const struct sockaddr* addr);
void to_ip(char* buf, size_t size, const struct sockaddr* addr);

void from_ip_port(const char* ip, uint16_t port, struct sockaddr_in* addr);
void from_ip_port(in_addr_t ip, uint16_t port, struct sockaddr_in* addr);
void from_ip_port(const char* ip, uint16_t port, struct sockaddr_in6* addr);

int getSocketError(int sockfd);

const struct sockaddr* sockaddr_cast(const struct sockaddr_in* addr);
const struct sockaddr* sockaddr_cast(const struct sockaddr_in6* addr);
struct sockaddr* sockaddr_cast(struct sockaddr_in6* addr);
const struct sockaddr_in* sockaddr_in_cast(const struct sockaddr* addr);
const struct sockaddr_in6* sockaddr_in6_cast(const struct sockaddr* addr);

bool is_self_connect(int sockfd);
struct sockaddr_in6 get_local_addr(int sockfd);
struct sockaddr_in6 get_peer_addr(int sockfd);

inline uint64_t host_to_be64(uint64_t host64) { return htobe64(host64); }
inline uint32_t host_to_be32(uint32_t host32) { return htobe32(host32); }
inline uint16_t host_to_be16(uint16_t host16) { return htobe16(host16); }
inline uint64_t be64_to_host(uint64_t net64) { return be64toh(net64); }
inline uint32_t be32_to_host(uint32_t net32) { return be32toh(net32); }
inline uint16_t be16_to_host(uint16_t net16) { return be16toh(net16); }

}  // namespace sock

#endif