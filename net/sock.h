#include <string>
#include <memory>

#include "util/status.h"

namespace net {

class SockAddrInetImpl;
class SocketImpl;

class SockAddrInet {
 public:
  std::string toString();

 private:
  std::unique_ptr<SockAddrInet> impl_;
};

class Socket {
 public:
  Status listen();
  Status bind();
  Status accept(SockAddrInet* addr) const;

  void shutdownWithWrite() {}

  void setReusePort(bool on) {}
  void setReuseAddr(bool on) {}
  void setKeepAlive(bool on) {}

 private:
  std::unique_ptr<SocketImpl> impl_;
};

class SockAddrInetImpl {
 virtual ~SockAddrInetImpl() = 0;
};

class SocketImpl {
 virtual ~SocketImpl() = 0;
};

} // namespace net