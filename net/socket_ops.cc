#include "socket_ops.h"

#include <cassert>
#include <cstring>

#include <sys/uio.h>

#include "util/logging.h"

const struct sockaddr* sock::sockaddr_cast(const struct sockaddr_in6* addr) {
  return static_cast<const struct sockaddr*>(static_cast<const void*>(addr));
}

struct sockaddr* sock::sockaddr_cast(struct sockaddr_in6* addr) {
  return static_cast<struct sockaddr*>(static_cast<void*>(addr));
}

const struct sockaddr* sock::sockaddr_cast(const struct sockaddr_in* addr) {
  return static_cast<const struct sockaddr*>(static_cast<const void*>(addr));
}

const struct sockaddr_in* sock::sockaddr_in_cast(const struct sockaddr* addr) {
  return static_cast<const struct sockaddr_in*>(static_cast<const void*>(addr));
}

const struct sockaddr_in6* sock::sockaddr_in6_cast(
    const struct sockaddr* addr) {
  return static_cast<const struct sockaddr_in6*>(
      static_cast<const void*>(addr));
}

int sock::create_sockfd_nonblocking(sa_family_t family) {
  int sockfd =
      ::socket(family, SOCK_STREAM | SOCK_NONBLOCK | SOCK_CLOEXEC, IPPROTO_TCP);

  if (sockfd < 0) {
    LOG(FATAL) << "sock::create_sockfd_nonblocking";
  }

  return sockfd;
}

void sock::bind_or_abort(int sockfd, const struct sockaddr* addr) {
  int ret =
      ::bind(sockfd, addr, static_cast<socklen_t>(sizeof(struct sockaddr_in6)));
  if (ret < 0) {
    LOG(FATAL) << "sock::bind_or_abort";
  }
}

void sock::listen_or_abort(int sockfd) {
  int ret = ::listen(sockfd, SOMAXCONN);
  if (ret < 0) {
    LOG(FATAL) << "sock::listen_or_abort";
  }
}

int sock::accept_or_abort(int sockfd, struct sockaddr_in6* addr) {
  socklen_t addrlen = static_cast<socklen_t>(sizeof *addr);

  int connfd = ::accept4(sockfd, sockaddr_cast(addr), &addrlen,
                         SOCK_NONBLOCK | SOCK_CLOEXEC);
  if (connfd < 0) {
    int saved_errno = errno;
    LOG(ERROR) << "sock::accept";
    switch (saved_errno) {
      case EAGAIN:
      case ECONNABORTED:
      case EINTR:
      case EPROTO:  // ???
      case EPERM:
      case EMFILE:  // per-process lmit of open file desctiptor ???
        // expected errors
        errno = saved_errno;
        break;
      case EBADF:
      case EFAULT:
      case EINVAL:
      case ENFILE:
      case ENOBUFS:
      case ENOMEM:
      case ENOTSOCK:
      case EOPNOTSUPP:
        // unexpected errors
        LOG(FATAL) << "unexpected error of ::accept " << saved_errno;
        break;
      default:
        LOG(FATAL) << "unknown error of ::accept " << saved_errno;
        break;
    }
  }
  return connfd;
}

int sock::create_bind_listen(int port) {
  int sockfd = sock::create_sockfd_nonblocking(AF_INET);
  sockaddr_in addr;
  in_addr_t ip = INADDR_ANY;
  sock::from_ip_port(ip, port, &addr);
  sock::bind_or_abort(sockfd, sock::sockaddr_cast(&addr));
  sock::listen_or_abort(sockfd);

  return sockfd;
}

int sock::connect(int sockfd, const struct sockaddr* addr) {
  return ::connect(sockfd, addr,
                   static_cast<socklen_t>(sizeof(struct sockaddr_in6)));
}

ssize_t sock::read(int sockfd, void* buf, size_t count) {
  return ::read(sockfd, buf, count);
}

ssize_t sock::readv(int sockfd, const struct iovec* iov, int iovcnt) {
  return ::readv(sockfd, iov, iovcnt);
}

ssize_t sock::write(int sockfd, const void* buf, size_t count) {
  return ::write(sockfd, buf, count);
}

void sock::close(int sockfd) {
  if (::close(sockfd) < 0) {
    LOG(ERROR) << "sock::close";
  }
}

void sock::shutdown_with_write(int sockfd) {
  if (::shutdown(sockfd, SHUT_WR) < 0) {
    LOG(ERROR) << "sock::shutdown_write";
  }
}

void sock::to_ip_port(char* buf, size_t size, const struct sockaddr* addr) {
  if (addr->sa_family == AF_INET6) {
    buf[0] = '[';
    to_ip(buf + 1, size - 1, addr);
    size_t end = ::strlen(buf);
    const struct sockaddr_in6* addr6 = sockaddr_in6_cast(addr);
    uint16_t port = sock::be16_to_host(addr6->sin6_port);
    assert(size > end);
    snprintf(buf + end, size - end, "]:%u", port);
    return;
  }
  to_ip(buf, size, addr);
  size_t end = ::strlen(buf);
  const struct sockaddr_in* addr4 = sockaddr_in_cast(addr);
  uint16_t port = sock::be16_to_host(addr4->sin_port);
  assert(size > end);
  snprintf(buf + end, size - end, ":%u", port);
}

void sock::to_ip(char* buf, size_t size, const struct sockaddr* addr) {
  if (addr->sa_family == AF_INET) {
    assert(size >= INET_ADDRSTRLEN);
    const struct sockaddr_in* addr4 = sockaddr_in_cast(addr);
    ::inet_ntop(AF_INET, &addr4->sin_addr, buf, static_cast<socklen_t>(size));
  } else if (addr->sa_family == AF_INET6) {
    assert(size >= INET6_ADDRSTRLEN);
    const struct sockaddr_in6* addr6 = sockaddr_in6_cast(addr);
    ::inet_ntop(AF_INET6, &addr6->sin6_addr, buf, static_cast<socklen_t>(size));
  }
}

void sock::from_ip_port(const char* ip, uint16_t port,
                        struct sockaddr_in* addr) {
  addr->sin_family = AF_INET;
  addr->sin_port = host_to_be16(port);
  if (::inet_pton(AF_INET, ip, &addr->sin_addr) <= 0) {
    LOG(ERROR) << "sock::from_ip_port";
  }
}

void sock::from_ip_port(in_addr_t ip, uint16_t port, struct sockaddr_in* addr) {
  addr->sin_family = AF_INET;
  addr->sin_port = host_to_be16(port);
  addr->sin_addr.s_addr = htonl(ip);
}

void sock::from_ip_port(const char* ip, uint16_t port,
                        struct sockaddr_in6* addr) {
  addr->sin6_family = AF_INET6;
  addr->sin6_port = host_to_be16(port);
  if (::inet_pton(AF_INET6, ip, &addr->sin6_addr) <= 0) {
    LOG(ERROR) << "sock::from_ip_port";
  }
}

struct sockaddr_in6 sock::get_local_addr(int sockfd) {
  struct sockaddr_in6 local_addr;
  bzero(&local_addr, sizeof(local_addr));
  socklen_t addrlen = static_cast<socklen_t>(sizeof(local_addr));

  if (::getsockname(sockfd, sockaddr_cast(&local_addr), &addrlen) < 0) {
    LOG(ERROR) << "sock::get_local_addr";
  }
  return local_addr;
}

struct sockaddr_in6 sock::get_peer_addr(int sockfd) {
  struct sockaddr_in6 peer_addr;
  bzero(&peer_addr, sizeof(peer_addr));
  socklen_t addrlen = static_cast<socklen_t>(sizeof(peer_addr));
  if (::getpeername(sockfd, sockaddr_cast(&peer_addr), &addrlen) < 0) {
    LOG(ERROR) << "sock::get_peer_addr";
  }
  return peer_addr;
}

bool sock::is_self_connect(int sockfd) {
  struct sockaddr_in6 local_addr = get_local_addr(sockfd);
  struct sockaddr_in6 peer_addr = get_peer_addr(sockfd);

  if (local_addr.sin6_family == AF_INET) {
    const struct sockaddr_in* laddr4 =
        reinterpret_cast<struct sockaddr_in*>(&local_addr);
    const struct sockaddr_in* raddr4 =
        reinterpret_cast<struct sockaddr_in*>(&peer_addr);
    return laddr4->sin_port == raddr4->sin_port &&
           laddr4->sin_addr.s_addr == raddr4->sin_addr.s_addr;

  } else if (local_addr.sin6_family == AF_INET6) {
    return local_addr.sin6_port == peer_addr.sin6_port &&
           memcmp(&local_addr.sin6_addr, &peer_addr.sin6_addr,
                  sizeof local_addr.sin6_addr) == 0;
  } else {
    return false;
  }
}