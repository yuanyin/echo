#ifndef NET_URL_H_
#define NET_URL_H_

#include <string>

namespace net {

struct URL {
  std::string scheme;
  std::string user;
  // std::string password;
  std::string host;
  int port;
  std::string path;      // /*/*
  std::string query;     // ?...
  std::string fragment;  // #...

 public:
  void reset();
  void clear() { reset(); }
  bool parse(const std::string&);
  bool parse(const char*, int size);
};

}  // namespace net

#endif