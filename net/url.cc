#include "url.h"

namespace net {

void URL::reset() {
  scheme.clear();
  user.clear();
  host.clear();
  path.clear();
  query.clear();
  fragment.clear();
}

bool URL::parse(const std::string& s) { return parse(s.c_str(), s.size()); }

enum URL_STATE {
  URL_START,
  URL_SCHEME,
  URL_FIRST_SLASH,
  URL_SECOND_SLASH,
  URL_COLON,  // 冒号
  URL_USER_HOST,
  URL_HOST,
  URL_PORT,
  URL_PATH,
  URL_QUERY,
  URL_FRAGMENT
};

bool URL::parse(const char* buf, int size) {
  URL_STATE state = URL_START;
  this->reset();

  port = 0;
  path = "";
  std::string tmp_user_host;
  std::string tmp_port;

  for (int i = 0; i < size; i++) {
    auto ch = buf[i];
    switch (state) {
      case URL_START:
        if (ch == '/') {
          state = URL_PATH;
          path += ch;
        } else if (std::isalpha(ch)) {
          state = URL_SCHEME;
          scheme += ch;
        } else {
          return false;
        }
        break;

      case URL_SCHEME:
        if (ch == ':') {
          state = URL_FIRST_SLASH;
        } else if (isalpha(ch)) {
          scheme += ch;
        } else {
          return false;
        }
        break;

      case URL_FIRST_SLASH:
        if (ch == '/') {
          state = URL_SECOND_SLASH;
        } else {
          return false;
        }
        break;

      case URL_SECOND_SLASH: {
        if (ch == '/') {
          state = URL_USER_HOST;
        } else {
          return false;
        }
        break;
      }

      case URL_USER_HOST: {
        if (ch == '@') {
          user = tmp_user_host;
          state = URL_HOST;
        } else if (ch == '/') {
          host = tmp_user_host;
          path += ch;
          state = URL_PATH;
        } else if (ch == ':') {
          host = tmp_user_host;
          state = URL_PORT;
        } else if (isalnum(ch) || ch == '.') {
          tmp_user_host += ch;
        } else {
          return false;
        }
        break;
      }

      case URL_HOST: {
        if (ch == '/') {
          path += ch;
          state = URL_PATH;
        } else if (ch == ':') {
          state = URL_PORT;
        } else {
          host += ch;
        }
        break;
      }

      case URL_PORT: {
        if (ch == '/') {
          state = URL_PATH;
        }
        break;
      }

      case URL_PATH: {
        if (ch == '?') {
          state = URL_QUERY;
        } else if (ch == '#') {
          state = URL_FRAGMENT;
        } else {
          path += ch;
        }
        break;
      }

      case URL_QUERY: {
        if (ch == '#') {
          state = URL_FRAGMENT;
        } else {
          query += ch;
        }
        break;
      }

      case URL_FRAGMENT: {
        fragment += ch;
        break;
      }

      default:
        return false;
        break;
    }
  }
  return true;
}

}  // namespace net