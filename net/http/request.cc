#include "request.h"

namespace http {

enum REQ_LINE_STATE {
  REQ_LINE_START,
  REQ_LINE_METHOD,
  REQ_LINE_URL,
  REQ_LINE_VERSION,
  REQ_LINE_FINISH
};

bool Request::parse_request_line() {
  auto& str = request_message_;
  auto off = 0;

  int pos = 0;
  std::string::size_type line_end = str.find('\r');
  if (line_end == str.npos) {
    LOG(ERROR) << "Parse request line error.";
    return false;
  }
  line_end += 1;

  if (line_end < str.size() && str[line_end] == '\n') {
  } else {
    return false;
  }

  REQ_LINE_STATE state = REQ_LINE_START;
  for (; off <= line_end; ++off) {
    auto ch = str[off];

    switch (state) {
      case REQ_LINE_START:
        if (std::isspace(ch)) {
          state = REQ_LINE_URL;
          method_ = str.substr(pos, off - pos);

          pos = off + 1;
        } else if (ch == '\r' || ch == '\n') {
          LOG(ERROR) << "Parse method error.";
          return false;
        } else {
        }
        break;

      case REQ_LINE_URL:
        if (std::isspace(ch)) {
          int ret = url_.parse(str.substr(pos, off - pos));
          pos = off + 1;
          if (!ret) {
            return false;
          }
          state = REQ_LINE_VERSION;
        } else if (ch == '\r' || ch == '\n') {
          // do nothing
        }
        break;

      case REQ_LINE_VERSION:
        if (ch == '\r') {
          state = REQ_LINE_FINISH;
          proto_ = str.substr(pos, off - pos);
        } else if (std::isalnum(ch) || ch == '.' || ch == '/') {
          break;
        } else {
          return false;
        }
        break;

      case REQ_LINE_FINISH:
        if (ch == '\n') {
          return true;
        } else {
          return false;
        }
        break;

      default:
        return false;
        break;
    }
  }

  if (state != REQ_LINE_FINISH) {
    return false;
  }

  return true;
}

bool Request::process_request_message() {
  request_message_.append(input_buffer_.begin(), input_buffer_.end());
  input_buffer_.skipAll();

  if (request_message_.empty()) {
    return false;
  }

  std::string::size_type pos;

  bool ret = parse_request_line();

  if (!ret) {
    LOG(ERROR) << "Parse request line error.";
    return false;
  }

  return true;
}

} // namespace http