#ifndef NET_HTTP_COOKIE_H_
#define NET_HTTP_COOKIE_H_

#include <string>
#include <vector>

#include "header.h"

namespace http {

struct Cookie {
  std::string name;
  std::string value;

  static std::string readSetCookies(Header& header_) {
    return std::string("No Implementation");
  }

  static void setCookie(Cookie *cookie) {
    // No Implementation
  }
};

} // namespace http

#endif