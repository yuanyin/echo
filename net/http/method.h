#ifndef NET_HTTP_METHOD_H_
#define NET_HTTP_METHOD_H_

#include <string>

namespace http {

const std::string MethodGet     = "GET";
const std::string MethodHead    = "HEAD";
const std::string MethodPost    = "POST";
const std::string MethodPut     = "PUT";
const std::string MethodPatch   = "PATCH"; // RFC 5790
const std::string MethodDelete  = "DELETE";
const std::string MethodConnect = "CONNECT";
const std::string MethodOptions = "OPTIONS";
const std::string MethodTrace   = "TRACE";

} // nanmespace http

#endif