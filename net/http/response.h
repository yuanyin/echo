#ifndef NET_HTTP_RESPONSE_H_
#define NET_HTTP_RESPONSE_H_

#include "request.h"

namespace http {

class Response {
private:
 Request *request_;

  // Headers
  int content_length_;
  bool keep_alive_;

  // Response
  int status_code_;

public:

  std::string response_message_;
  ChannelBuffer output_buffer_;


  Response(Request *req) : request_(req) {}

  std::string getCookie(std::string key) {
    if (request_->cookies_.count(key)) {
      return request_->cookies_[key];
    }
    return std::string();
  }

  void setCookie(std::string key, std::string value) { request_->cookies_[key] = value; }

  // response
  void process_response_message();
  void add_status_line(int code);
  void add_headers() {}
  void add_content_type(const std::string& media_type,
                        const std::string& charset = std::string(),
                        const std::string& boundary = std::string()) {
    response_message_ += HeaderContentType + ": " + media_type;
    if (!charset.empty()) {
      response_message_ += "; charset=" + charset;
    }
    if (!boundary.empty()) {
      response_message_ += "; boundary=" + boundary;
    }
    response_message_ += LINE_END;
  }
  void add_content_length(int length) {
    response_message_ += HeaderContentLength + ": " + std::to_string(length) + LINE_END;
  }

  void add_cookies() {
    response_message_ += HeaderSetCookie + ": ";
    bool is_start = true;

    for (auto& [key, value] : request_->cookies_) {  // C++17
      if (is_start) {
        is_start = false;
      } else {
        response_message_ += "; ";
      }
      response_message_ += key + "=" + value;
    }

    response_message_ += LINE_END;
  }
  void add_connection_keep_alive() {
    response_message_ += HeaderConnection + ": Keep-Alive\r\n";
  }
  void add_keep_alive(int timeout = 5, int max = 1000) {
    response_message_ += "timeout=" + std::to_string(timeout) + ", " + "max=" + std::to_string(max) + "\r\n";
  }
  void add_blank_line() { response_message_ += LINE_END; }

  void add_header(std::string key, std::string value) {
    response_message_ += key + ": " + value + LINE_END;
  }

  int addFile(const std::filesystem::path& file_path);

  void sendEcho();


};

} // namespace http

#endif