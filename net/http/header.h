#ifndef NET_HTTP_HEADER_
#define NET_HTTP_HEADER_

#include <string>
#include <vector>
#include <unordered_map>

#include "io/io.h"

namespace http {

const std::string HeaderAccept         = "Accept";
const std::string HeaderAcceptEncoding = "Accept-Encoding";
// HeaderAllow is the name of the "Allow" header field used to list the set of methods
// advertised as supported by the target resource. Returning an Allow header is mandatory
// for status 405 (method not found) and useful for the OPTIONS method in responses.
// See RFC 7231: https://datatracker.ietf.org/doc/html/rfc7231#section-7.4.1
const std::string HeaderAllow               = "Allow";
const std::string HeaderAuthorization       = "Authorization";
const std::string HeaderContentDisposition  = "Content-Disposition";
const std::string HeaderContentEncoding     = "Content-Encoding";
const std::string HeaderContentLength       = "Content-Length";
const std::string HeaderContentType         = "Content-Type";
const std::string HeaderCookie              = "Cookie";
const std::string HeaderSetCookie           = "Set-Cookie";
const std::string HeaderIfModifiedSince     = "If-Modified-Since";
const std::string HeaderLastModified        = "Last-Modified";
const std::string HeaderLocation            = "Location";
const std::string HeaderRetryAfter          = "Retry-After";
const std::string HeaderUpgrade             = "Upgrade";
const std::string HeaderVary                = "Vary";
const std::string HeaderWWWAuthenticate     = "WWW-Authenticate";
const std::string HeaderXForwardedFor       = "X-Forwarded-For";
const std::string HeaderXForwardedProto     = "X-Forwarded-Proto";
const std::string HeaderXForwardedProtocol  = "X-Forwarded-Protocol";
const std::string HeaderXForwardedSsl       = "X-Forwarded-Ssl";
const std::string HeaderXUrlScheme          = "X-Url-Scheme";
const std::string HeaderXHTTPMethodOverride = "X-HTTP-Method-Override";
const std::string HeaderXRealIP             = "X-Real-Ip";
const std::string HeaderXRequestID          = "X-Request-Id";
const std::string HeaderXCorrelationID      = "X-Correlation-Id";
const std::string HeaderXRequestedWith      = "X-Requested-With";
const std::string HeaderServer              = "Server";
const std::string HeaderOrigin              = "Origin";
const std::string HeaderCacheControl        = "Cache-Control";
const std::string HeaderConnection          = "Connection";

// Access control
const std::string HeaderAccessControlRequestMethod    = "Access-Control-Request-Method";
const std::string HeaderAccessControlRequestHeaders   = "Access-Control-Request-Headers";
const std::string HeaderAccessControlAllowOrigin      = "Access-Control-Allow-Origin";
const std::string HeaderAccessControlAllowMethods     = "Access-Control-Allow-Methods";
const std::string HeaderAccessControlAllowHeaders     = "Access-Control-Allow-Headers";
const std::string HeaderAccessControlAllowCredentials = "Access-Control-Allow-Credentials";
const std::string HeaderAccessControlExposeHeaders    = "Access-Control-Expose-Headers";
const std::string HeaderAccessControlMaxAge           = "Access-Control-Max-Age";

// Security
const std::string HeaderStrictTransportSecurity         = "Strict-Transport-Security";
const std::string HeaderXContentTypeOptions             = "X-Content-Type-Options";
const std::string HeaderXXSSProtection                  = "X-XSS-Protection";
const std::string HeaderXFrameOptions                   = "X-Frame-Options";
const std::string HeaderContentSecurityPolicy           = "Content-Security-Policy";
const std::string HeaderContentSecurityPolicyReportOnly = "Content-Security-Policy-Report-Only";
const std::string HeaderXCSRFToken                      = "X-CSRF-Token";
const std::string HeaderReferrerPolicy                  = "Referrer-Policy";

class Header {
 public:
  typedef std::vector<std::string> StringList;
  typedef std::string String;
  typedef std::unordered_map<String, StringList> StringListMap;

  void add(std::string key, std::string value) {
      header_[key].emplace_back(value);
  }

  void set(std::string key, std::string value) {
      header_[key] = {value};
  }

  std::string get(std::string key) {
      if (!header_.count(key)) {
          return "";
      }

      if (header_[key].empty()) {
          return "";
      }

      return header_[key][0];
  }

  StringList values(std::string key) {
    if (!header_.count(key)) {
      return StringList();
    }
    return header_[key];
  }

  bool has(std::string key) {
    return header_.count(key);
  }

  void del(std::string key) {
      header_.erase(key);
  }

  void write(Writer& w) {
    for (auto & [k, vs] : header_) {
        for (auto & v : vs) {
            w.write(k.c_str(), k.size());
            w.write(": ", 2);
            w.write(v.c_str(), v.size());
            w.write("\r\n", 2);
        }
    }
  }

 private:
  StringListMap header_;
};

} // namespace http

#endif