#ifndef NET_HTTP_STATUS_H_
#define NET_HTTP_STATUS_H_

#include <unordered_map>
#include <string>

namespace http {

// See: https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
const int StatusContinue           = 100; // RFC 7231, 6.2.1
const int StatusSwitchingProtocols = 101; // RFC 7231, 6.2.2
const int StatusProcessing         = 102; // RFC 2518, 10.1
const int StatusEarlyHints         = 103; // RFC 8297

const int StatusOK                   = 200; // RFC 7231, 6.3.1
const int StatusCreated              = 201; // RFC 7231, 6.3.2
const int StatusAccepted             = 202; // RFC 7231, 6.3.3
const int StatusNonAuthoritativeInfo = 203; // RFC 7231, 6.3.4
const int StatusNoContent            = 204; // RFC 7231, 6.3.5
const int StatusResetContent         = 205; // RFC 7231, 6.3.6
const int StatusPartialContent       = 206; // RFC 7233, 4.1
const int StatusMultiStatus          = 207; // RFC 4918, 11.1
const int StatusAlreadyReported      = 208; // RFC 5842, 7.1
const int StatusIMUsed               = 226; // RFC 3229, 10.4.1

const int StatusMultipleChoices   = 300; // RFC 7231, 6.4.1
const int StatusMovedPermanently  = 301; // RFC 7231, 6.4.2
const int StatusFound             = 302; // RFC 7231, 6.4.3
const int StatusSeeOther          = 303; // RFC 7231, 6.4.4
const int StatusNotModified       = 304; // RFC 7232, 4.1
const int StatusUseProxy          = 305; // RFC 7231, 6.4.5
const int _                       = 306; // RFC 7231, 6.4.6 (Unused)
const int StatusTemporaryRedirect = 307; // RFC 7231, 6.4.7
const int StatusPermanentRedirect = 308; // RFC 7538, 3

const int StatusBadRequest                   = 400; // RFC 7231, 6.5.1
const int StatusUnauthorized                 = 401; // RFC 7235, 3.1
const int StatusPaymentRequired              = 402; // RFC 7231, 6.5.2
const int StatusForbidden                    = 403; // RFC 7231, 6.5.3
const int StatusNotFound                     = 404; // RFC 7231, 6.5.4
const int StatusMethodNotAllowed             = 405; // RFC 7231, 6.5.5
const int StatusNotAcceptable                = 406; // RFC 7231, 6.5.6
const int StatusProxyAuthRequired            = 407; // RFC 7235, 3.2
const int StatusRequestTimeout               = 408; // RFC 7231, 6.5.7
const int StatusConflict                     = 409; // RFC 7231, 6.5.8
const int StatusGone                         = 410; // RFC 7231, 6.5.9
const int StatusLengthRequired               = 411; // RFC 7231, 6.5.10
const int StatusPreconditionFailed           = 412; // RFC 7232, 4.2
const int StatusRequestEntityTooLarge        = 413; // RFC 7231, 6.5.11
const int StatusRequestURITooLong            = 414; // RFC 7231, 6.5.12
const int StatusUnsupportedMediaType         = 415; // RFC 7231, 6.5.13
const int StatusRequestedRangeNotSatisfiable = 416; // RFC 7233, 4.4
const int StatusExpectationFailed            = 417; // RFC 7231, 6.5.14
const int StatusTeapot                       = 418; // RFC 7168, 2.3.3
const int StatusMisdirectedRequest           = 421; // RFC 7540, 9.1.2
const int StatusUnprocessableEntity          = 422; // RFC 4918, 11.2
const int StatusLocked                       = 423; // RFC 4918, 11.3
const int StatusFailedDependency             = 424; // RFC 4918, 11.4
const int StatusTooEarly                     = 425; // RFC 8470, 5.2.
const int StatusUpgradeRequired              = 426; // RFC 7231, 6.5.15
const int StatusPreconditionRequired         = 428; // RFC 6585, 3
const int StatusTooManyRequests              = 429; // RFC 6585, 4
const int StatusRequestHeaderFieldsTooLarge  = 431; // RFC 6585, 5
const int StatusUnavailableForLegalReasons   = 451; // RFC 7725, 3

const int StatusInternalServerError           = 500; // RFC 7231, 6.6.1
const int StatusNotImplemented                = 501; // RFC 7231, 6.6.2
const int StatusBadGateway                    = 502; // RFC 7231, 6.6.3
const int StatusServiceUnavailable            = 503; // RFC 7231, 6.6.4
const int StatusGatewayTimeout                = 504; // RFC 7231, 6.6.5
const int StatusHTTPVersionNotSupported       = 505; // RFC 7231, 6.6.6
const int StatusVariantAlsoNegotiates         = 506; // RFC 2295, 8.1
const int StatusInsufficientStorage           = 507; // RFC 4918, 11.5
const int StatusLoopDetected                  = 508; // RFC 5842, 7.2
const int StatusNotExtended                   = 510; // RFC 2774, 7
const int StatusNetworkAuthenticationRequired = 511; // RFC 6585, 6

const std::unordered_map<int, std::string> kStatusText {
  { StatusContinue,           "Continue"},
  { StatusSwitchingProtocols, "Switching Protocols"},
  { StatusProcessing,         "Processing"},
  { StatusEarlyHints,         "Early Hints"},
   
  { StatusOK,                   "OK"},
  { StatusCreated,              "Created"},
  { StatusAccepted,             "Accepted"},
  { StatusNonAuthoritativeInfo, "Non-Authoritative Information"},
  { StatusNoContent,            "No Content"},
  { StatusResetContent,         "Reset Content"},
  { StatusPartialContent,       "Partial Content"},
  { StatusMultiStatus,          "Multi-Status"},
  { StatusAlreadyReported,      "Already Reported"},
  { StatusIMUsed,               "IM Used"},
   
  { StatusMultipleChoices,   "Multiple Choices"},
  { StatusMovedPermanently,  "Moved Permanently"},
  { StatusFound,             "Found"},
  { StatusSeeOther,          "See Other"},
  { StatusNotModified,       "Not Modified"},
  { StatusUseProxy,          "Use Proxy"},
  { StatusTemporaryRedirect, "Temporary Redirect"},
  { StatusPermanentRedirect, "Permanent Redirect"},
   
  { StatusBadRequest,                   "Bad Request"},
  { StatusUnauthorized,                 "Unauthorized"},
  { StatusPaymentRequired,              "Payment Required"},
  { StatusForbidden,                    "Forbidden"},
  { StatusNotFound,                     "Not Found"},
  { StatusMethodNotAllowed,             "Method Not Allowed"},
  { StatusNotAcceptable,                "Not Acceptable"},
  { StatusProxyAuthRequired,            "Proxy Authentication Required"},
  { StatusRequestTimeout,               "Request Timeout"},
  { StatusConflict,                     "Conflict"},
  { StatusGone,                         "Gone"},
  { StatusLengthRequired,               "Length Required"},
  { StatusPreconditionFailed,           "Precondition Failed"},
  { StatusRequestEntityTooLarge,        "Request Entity Too Large"},
  { StatusRequestURITooLong,            "Request URI Too Long"},
  { StatusUnsupportedMediaType,         "Unsupported Media Type"},
  { StatusRequestedRangeNotSatisfiable, "Requested Range Not Satisfiable"},
  { StatusExpectationFailed,            "Expectation Failed"},
  { StatusTeapot,                       "I'm a teapot"},
  { StatusMisdirectedRequest,           "Misdirected Request"},
  { StatusUnprocessableEntity,          "Unprocessable Entity"},
  { StatusLocked,                       "Locked"},
  { StatusFailedDependency,             "Failed Dependency"},
  { StatusTooEarly,                     "Too Early"},
  { StatusUpgradeRequired,              "Upgrade Required"},
  { StatusPreconditionRequired,         "Precondition Required"},
  { StatusTooManyRequests,              "Too Many Requests"},
  { StatusRequestHeaderFieldsTooLarge,  "Request Header Fields Too Large"},
  { StatusUnavailableForLegalReasons,   "Unavailable For Legal Reasons"},
   
  { StatusInternalServerError,           "Internal Server Error"},
  { StatusNotImplemented,                "Not Implemented"},
  { StatusBadGateway,                    "Bad Gateway"},
  { StatusServiceUnavailable,            "Service Unavailable"},
  { StatusGatewayTimeout,                "Gateway Timeout"},
  { StatusHTTPVersionNotSupported,       "HTTP Version Not Supported"},
  { StatusVariantAlsoNegotiates,         "Variant Also Negotiates"},
  { StatusInsufficientStorage,           "Insufficient Storage"},
  { StatusLoopDetected,                  "Loop Detected"},
  { StatusNotExtended,                   "Not Extended"},
  { StatusNetworkAuthenticationRequired, "Network Authentication Required"}
};

// StatusText returns a text for the HTTP status code. It returns the empty
// string if the code is unknown.
inline std::string StatusText(int code) {
	return kStatusText.at(code);
}

} // namespace http

#endif