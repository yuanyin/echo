#ifndef NET_HTTP_REQUEST_H_
#define NET_HTTP_REQUEST_H_

#include <filesystem>
#include <functional>
#include <map>
#include <memory>

#include "status.h"
#include "header.h"
#include "method.h"

#include "mime/type.h"
#include "net/url.h"
#include "io/buffer.h"
#include "io/channel.h"



namespace http {


const char http_line_end[] = "\r\n";
const char LINE_END[] = "\r\n";

class Request {
 public:
  Request() = default;

  // Internal tool method
  // request
  bool process_request_message();
  bool parse_request_line();

  std::string request_message_;
  ChannelBuffer input_buffer_;

  std::string method_;
  net::URL url_;
  std::string proto_;
  int proto_major_;
  int proto_minor_;

  // Request Line
  http::Header header_;
  std::map<std::string, std::string> cookies_;
};

} // namespace http

#endif