#include "response.h"

namespace http {

#define SESSIONID_LEN 52
std::string generateSessionId() {
  std::srand(std::time(nullptr));
  std::string session_id;
  session_id.resize(SESSIONID_LEN);

  int n = 0;
  while (n < SESSIONID_LEN) {
    char ch = std::rand() % 127;
    if (isalnum(ch)) {
      session_id[n++] = ch;
    }
  }

  return session_id;
}

std::string strToHtml(const std::string& s) {
  std::string ret;
  int pos = 0, off = 0;
  while (off < s.size()) {
    pos = s.find('\r', off);
    if (pos == s.size()) {
      ret += s.substr(off);
      return ret;
    }
    ret += s.substr(off, pos - off);
    ret += "</br>";
    off = pos + 2;
  }
  return ret;
}

void Response::add_status_line(int code) {
  response_message_ += request_->proto_ + " ";
  response_message_ += std::to_string(code) + " ";
  response_message_ += http::StatusText(code) + "\r\n";
}

int Response::addFile(const std::filesystem::path& file_path) {
  int origin_size = response_message_.size();

  int file_size = std::filesystem::file_size(file_path);

  FileReader file_reader(file_path);
  auto offset = response_message_.size();
  response_message_.append(file_size, 0);
  auto status = file_reader.read(&response_message_[offset], file_size);

  if (status.isOK()) {
    LOG(TRACE) << "Add send file: " << file_path.c_str() << "success";
  }

  return file_size;
}

void Response::process_response_message() {
  std::filesystem::path file_path = "static";
  try {
    if (!std::filesystem::exists(file_path)) {
      sendEcho();
      return;
    }

    file_path.concat(request_->url_.path);
    if (!std::filesystem::exists(file_path)) {
      status_code_ = 400;
      sendEcho();
      return;
    }

    if (std::filesystem::is_directory(file_path)) {
      file_path.concat("index.html");
    }

  } catch (std::exception& e) {
    std::cout << e.what() << std::endl;
  }

  if (std::filesystem::exists(file_path)) {
    add_status_line(200);

    if (!request_->cookies_.count("SESSIONID")) {
      setCookie("SESSIONID", generateSessionId());
      add_cookies();
    }

    int file_size = std::filesystem::file_size(file_path);

    std::string mime_type;
    if (file_path.has_extension()) {
      auto ext = file_path.extension();
      mime_type = mime::getTypeByExtension(ext.c_str());
    } else {
      mime_type = "text/plain";
    }

    add_connection_keep_alive();
    add_keep_alive();

    add_content_type(mime_type);
    add_content_length(file_size);
    add_header("Server", "Echo by Yuanyin Zhang");
    add_header("X-Content-Type-Options", "nosniff");

    add_blank_line();

    addFile(file_path);
  } else {
    sendEcho();
  }
}

void Response::sendEcho() {
  status_code_ = 400;

  add_status_line(200);

  if (!request_->cookies_.count("SESSIONID")) {
    setCookie("SESSIONID", generateSessionId());
    add_cookies();
  }

  std::string content = "<html>";
  content += "<head> <title> echo </title> </head>";
  content += "<body> <h1>Echo Server</h1>";
  content += "<h2>Request Info</h2>";
  content += strToHtml(request_->request_message_);
  content += "<h2>Response Info</h2>";
  content += strToHtml(response_message_);
  content += "Content-Length: ";
  int hold_position = content.size();
  content += std::string(20, ' ');
  content += "</br>";
  content += "</body></html>";

  auto size_str = std::to_string(content.size());
  content = content.replace(hold_position, size_str.size(), size_str);

  add_content_type("text/html");
  add_content_length(content.size());
  add_blank_line();

  response_message_ += content;
}

} // namespace http