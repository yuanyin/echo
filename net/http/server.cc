#include "server.h"

#include <cassert>
#include <cstring>
#include <random>

#include "util/logging.h"
#include "io/io.h"


namespace http {

void HttpServer::handleRequest(Request* request) {
  int saved_errno;
  while (true) {
    int ret = request->input_buffer_.recvFrom(channel_->sock(), &saved_errno);
    if (ret < 0) {
      if (saved_errno == EINTR) {
        continue;
      } else if (saved_errno == EAGAIN) {
        break;
      } else {
        perror("read");
        break;
      }
    } else if (ret == 0) {
      break;
    }
  }

  LOG(TRACE) << "Do request " << "for fd " << channel_->sock();
  request->process_request_message();

  if (request->request_message_.empty()) {
    return;
  }
}

bool HttpServer::handleResponse(Response* resp) {
  const char* buf = resp->response_message_.data();
  int send_size = 0;
  int remain_size = resp->response_message_.size();

  while (remain_size > 0) {
    int ret =
        send(channel_->sock(), buf + send_size, remain_size, MSG_NOSIGNAL);

    if (ret < 0) {
      if (ret == EINTR) {
        continue;
      }
      perror("epoll->send");
      break;
    } else if (ret == 0) {
      // break;
    }
    send_size += ret;
    remain_size -= ret;
  }

  if (remain_size > 0) {
    LOG(TRACE) << "Non finish send action";
    return false;
  }

  LOG(TRACE) << "[fd-" << channel_->sock() << "] "
             << "Finished send " << send_size << "/" << resp->response_message_.size()
             << " bytes";

  return true;
}

void HttpServer::service() {
  auto request = std::make_unique<Request>();
  handleRequest(request.get());

  if (request->method_ == http::MethodGet) {
    auto response = std::make_unique<Response>(request.get());
    response->process_response_message();
    handleResponse(response.get());
  }
}

} // namespace http