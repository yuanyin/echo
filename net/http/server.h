#ifndef NET_HTTP_SERVER_H_
#define NET_HTTP_SERVER_H_

#include <errno.h>

#include <filesystem>
#include <functional>
#include <map>
#include <memory>

#include "status.h"
#include "header.h"
#include "method.h"
#include "request.h"
#include "response.h"

#include "mime/type.h"
#include "net/url.h"
#include "io/buffer.h"
#include "io/channel.h"

namespace http {

class HttpServer {
 public:
  HttpServer(std::shared_ptr<Channel> channel) : channel_(channel) {}

  void service();
  void handleRequest(Request*);
  bool handleResponse(Response* resp);

 private:
  // Connection
  std::shared_ptr<Channel> channel_;

};

typedef std::shared_ptr<HttpServer> HttpServerPtr;

} // namespace http

#endif