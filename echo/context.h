#ifndef ECHO_CONTEXT_H_
#define ECHO_CONTEXT_H_

#include <string>
#include <shared_mutex>

#include "net/http.h"

namespace echo {

class Echo;

class Context {
  http::Request request();
  http::Response response();
  void setRequest(http::Request *req);
  void setResponse(http::Response *resp);

  std::string path();
  std::string setPath();

  std::string scheme();

  std::string param(std::string name);
  std::string paramNames();
  std::string paramValues();
  void setParamValues();

  std::string queryParam(std::string name);

  http::Cookie cookie(std::string name);
  void SetCookie(http::Cookie);

  void sendHTML(int code, std::string html);
  void sendHTMLBlob(int code, const char* buf);
  void sendString();
  void sendJSON();
  void sendFile();
  void sendError();

 private:
  http::Request request_;
  http::Response response_;
  std::string path_;
  Echo *echo_;

  std::shared_mutex rwmu_;
};

} // namespace echo

#endif