#include "echo.h"

#include <iostream>
#include "io/ioutil.h"
#include "net/socket_ops.h"
#include "util/logging.h"

namespace echo {

const char banner[] = R"+*(
 _______   ________  ___  ___  ________
|\  ___ \ |\   ____\|\  \|\  \|\   __  \
\ \   __/|\ \  \___|\ \  \\\  \ \  \|\  \
 \ \  \_|/_\ \  \    \ \   __  \ \  \\\  \
  \ \  \_|\ \ \  \____\ \  \ \  \ \  \\\  \
   \ \_______\ \_______\ \__\ \__\ \_______\
    \|_______|\|_______|\|__|\|__|\|_______|
)+*";

void Echo::configure() {
  std::cout << banner << std::endl;
  listen_sock_ = sock::create_bind_listen(port_);
  sockaddr_in6 local_addr = sock::get_local_addr(listen_sock_);

  const size_t ip_port_buf_size = 30;
  char ip_port_buf[ip_port_buf_size];
  sock::to_ip_port(ip_port_buf, ip_port_buf_size,
                   sock::sockaddr_cast(&local_addr));

  std::cout << "http server started on " << ip_port_buf << std::endl;
}

void Echo::newConnHandler(Selector *selector) {
  sockaddr_in6 addr;
  int conn_sock = sock::accept_or_abort(listen_sock_, &addr);
  if (conn_sock == -1) {
    perror("accept");
    exit(EXIT_FAILURE);
  }

  char addr_str[INET_ADDRSTRLEN];
  ::inet_ntop(AF_INET, &(addr.sin6_addr), addr_str, INET_ADDRSTRLEN);
  LOG(INFO) << "New connection: client address is " << addr_str
            << " and fd " << conn_sock << " is allocated.";

  if (!channel_store_.count(conn_sock)) {
    channel_store_[conn_sock] = std::make_shared<Channel>(conn_sock);
  }

  if (!http_server_store_.count(conn_sock)) {
    http_server_store_[conn_sock] =
        std::make_shared<http::HttpServer>(channel_store_[conn_sock]);
  }

  auto channel = channel_store_[conn_sock].get();
  auto http_data = http_server_store_[conn_sock].get();

  if (channel == nullptr) {
    LOG(ERROR) << "Allocate failed: channel object is null.";
  }

  if (http_data == nullptr) {
    LOG(ERROR) << "Allocate failed: http data object is null.";
  }

  channel->setReadHandler([http_data]() { http_data->service(); });

  channel->setCloseHandler([&channel] {
    shutdown(channel->sock(), 2);
    close(channel->sock());

    LOG(INFO) << "Connection closed: client fd is" << channel->sock()
              << ".";
  });

  set_nonblocking(conn_sock);

  channel->setEvent(EPOLLIN | EPOLLET);
  selector->insertChannel(channel);
}

void Echo::start(int port) {
  port_ = port;
  this->configure();

  Selector selector;

  channel_store_[listen_sock_] = std::make_shared<Channel>(listen_sock_);
  auto listen_channel = channel_store_[listen_sock_];
  listen_channel->setEvent(EPOLLIN);

  auto selector_ptr = &selector;
  listen_channel->setReadHandler([this, selector_ptr] () { this->newConnHandler(selector_ptr); } );

  selector.insertChannel(listen_channel.get());

  for (;;) {
    ChannelList active_channel_list;
    selector.select(&active_channel_list);
    int nfds = active_channel_list.size();

    for (auto active_channel : active_channel_list) {
        int sock = active_channel->fd();

        if (!channel_store_.count(sock)) {
          continue;
        }

        auto channel = channel_store_[sock].get();
        if (channel == nullptr) {
          continue;
        }

        // 并发执行
        thread_pool_.run([channel]() { channel->handleEvent(); });
    }
  }
};

void Echo::startTLS(int port) {
  port_ = port;
  this->configure();
  LOG(ERROR) << "HTTPS is not completed yet";
}

void Echo::control(METHOD method, std::string path, std::function<void()>) {
  if (method == GET) {
  } else if (method == POST) {
  } else {
  }
}

void Echo::control(std::string method, std::string path,
                   std::function<void()>) {
  if (method == "GET") {
  } else if (method == "POST") {
  } else {
  }
}

} // namespace echo