#ifndef ECHO_H_
#define ECHO_H_

#include <map>
#include <memory>
#include <functional>
#include <cassert>

#include "net/http.h"
#include "io/channel.h"
#include "io/selector.h"
#include "util/thread_pool.h"

#include "context.h"

namespace echo {

typedef std::function<void(Context*)> HandlerFunc;

class Echo {
 public:
  enum METHOD { GET, POST, HEAD };

 public:
  Echo() : thread_pool_(8) {}

  void newConnHandler(Selector *);

  void start(int port = 8080);
  void startTLS(int port = 4433);
  void configure();

  void control(METHOD method, std::string path, std::function<void()>);
  void control(std::string method, std::string path, std::function<void()>);
  void process(int client_sock);

 private:
  int port_;
  int listen_sock_;

  ThreadPool thread_pool_;

  std::unordered_map<int, ChannelPtr>  channel_store_;
  std::unordered_map<int, http::HttpServerPtr> http_server_store_;
};

} // namespace echo

#endif