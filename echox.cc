#include <cassert>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <random>
#include <sstream>

#include "echo/echo.h"

int port = 8080;

int main(int argc, char *argv[]) {
  if (argc >= 2) {
    port = ::atoi(argv[1]);
  }

  echo::Echo e;

  e.start(port);
}