#include "type.h"

#include <algorithm>
#include <cctype>
#include <mutex>
#include <thread>

namespace mime {

std::once_flag flag;

StringMap kMimeTypes;
StringMap kMimeTypesLower;
StringListMap kExtensions;
std::mutex kExtensionsMu;

const StringMap kBuiltinTypesLower = {
    {".avif", "image/avif"},
    {".css", "text/css; charset=utf-8"},
    {".gif", "image/gif"},
    {".htm", "text/html; charset=utf-8"},
    {".html", "text/html; charset=utf-8"},
    {".jpeg", "image/jpeg"},
    {".jpg", "image/jpeg"},
    {".js", "text/javascript; charset=utf-8"},
    {".json", "application/json"},
    {".mjs", "text/javascript; charset=utf-8"},
    {".pdf", "application/pdf"},
    {".png", "image/png"},
    {".svg", "image/svg+xml"},
    {".wasm", "application/wasm"},
    {".webp", "image/webp"},
    {".xml", "text/xml; charset=utf-8"},
};

void setMimeTypes(StringMap lower_ext, StringMap mix_ext) {
  kMimeTypes.clear();
  kMimeTypesLower.clear();
  kExtensions.clear();

  for (auto [k, v] : lower_ext) {
    kMimeTypesLower.emplace(k, v);
  }
  for (auto [k, v] : mix_ext) {
    kMimeTypes.emplace(k, v);
  }
}

void initMime() { setMimeTypes(kBuiltinTypesLower, kBuiltinTypesLower); }

std::string getTypeByExtension(std::string ext) {
  std::call_once(flag, initMime);

  if (kMimeTypes.count(ext)) {
    return kMimeTypes[ext];
  }

  std::string lower = ext;
  std::transform(lower.begin(), lower.end(), lower.begin(), ::tolower);

  if (kMimeTypesLower.count(lower)) {
    return kMimeTypesLower[lower];
  }

  return std::string("text/plain");
}

StringList getExtensionsByType(std::string type) {
  std::call_once(flag, initMime);

  if (kExtensions.count(type)) {
    return kExtensions[type];
  }

  return StringList();
}

void setExtensionType(std::string ext, std::string typ) {
  std::lock_guard<std::mutex> lock(kExtensionsMu);
}

void addExtensionType(std::string ext, std::string typ) {
  if (ext.find(".") != 0) {
    return;
  }
  std::call_once(flag, initMime);
  kMimeTypes[ext] = typ;
  std::string lower = ext;
  std::transform(lower.begin(), lower.end(), lower.begin(), ::tolower);
  kMimeTypesLower[lower] = typ;

  setExtensionType(ext, typ);
}

}  // namespace mime