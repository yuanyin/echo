#ifndef MIME_TYPE_H_
#define MIME_TYPE_H_

#include <string>
#include <unordered_map>
#include <vector>

namespace mime {

typedef std::vector<std::string> StringList;
typedef std::unordered_map<std::string, std::string> StringMap;
typedef std::unordered_map<std::string, StringList> StringListMap;

void setMimeTypes(StringMap lower_ext, StringMap mix_ext);

void initMime();

std::string getTypeByExtension(std::string ext);

StringList getExtensionsByType(std::string type);

void addExtensionType(std::string ext, std::string typ);

}  // namespace mime

#endif