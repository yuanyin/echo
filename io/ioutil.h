#ifndef IO_OPS_H_
#define IO_OPS_H_

#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/epoll.h>
#include <unistd.h>

void set_nonblocking(int fd) {
  auto flag = fcntl(fd, F_GETFL);
  flag |= O_NONBLOCK;  // == SOCK_NONBLOCK
  fcntl(fd, F_SETFL, flag);
}

#endif