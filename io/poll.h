#ifndef IO_POLL_H_
#define IO_POLL_H_

#include "channel.h"

class Poll {
public:
  Poll() = default;
  ~Poll() = default;

  void poll(ChannelList*);
};

#endif