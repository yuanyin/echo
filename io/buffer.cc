#include "buffer.h"

#include <sys/uio.h>

#include "net/socket_ops.h"

const char ChannelBuffer::CRLF[] = "\r\n";

size_t ChannelBuffer::read(char *buf, size_t count) {
  if (count <= 0) return 0;
  size_t read_size = std::min(count, readableSize());
  std::copy(readerZone(), readerZone() + read_size, buf);
  readerIndexShift(read_size);
  return read_size;
}

size_t ChannelBuffer::recvFrom(int fd, int *saved_errno) {
  char extra_buf[65536];
  const size_t writable_size = writableSize();

  struct iovec vec[2];
  vec[0].iov_base = data() + writer_index_;
  vec[0].iov_len = writable_size;
  vec[1].iov_base = extra_buf;
  vec[1].iov_len = sizeof(extra_buf);

  const int iov_cnt = (writable_size < sizeof(extra_buf)) ? 2 : 1;
  const ssize_t n = sock::readv(fd, vec, iov_cnt);
  if (n < 0) {
    *saved_errno = errno;
  } else if (static_cast<size_t>(n) <= writable_size) {
    writer_index_ += n;
  } else {
    writer_index_ = buffer_.size();
    write(extra_buf, n - writable_size);
  }

  return n;
}

size_t ChannelBuffer::sendTo(int fd, int *saved_errno) {
  const ssize_t n = sock::write(fd, readerZone(), readableSize());
  if (n < 0) {
    *saved_errno = errno;
  } else if (n > 0) {
    skip(n);
  }

  return n;
}