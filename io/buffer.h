#ifndef BUFFER_H_
#define BUFFER_H_

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <cstring>
#include <string>
#include <vector>

#include "util/logging.h"

/// A buffer class like org.jboss.netty.buffer.ChannelChannelBuffer
///
/// +-------------------+------------------+------------------+
/// | discardable bytes |  readable bytes  |  writable bytes  |
/// |                   |     (CONTENT)    |                  |
/// +-------------------+------------------+------------------+
/// |                   |                  |                  |
/// 0      <=      readerIndex   <=   writerIndex    <=     capacity

class Buffer {};

class ChannelBuffer : public Buffer {
 public:
  static const size_t kInitialSize = 1024;
  static const char CRLF[];

  explicit ChannelBuffer(size_t initial_size = kInitialSize)
      : buffer_(initial_size), reader_index_(0), writer_index_(0) {
    assert(readableSize() == 0);
    assert(writableSize() == initial_size);
    assert(discardableSize() == 0);
  }

  size_t readableSize() { return writer_index_ - reader_index_; }
  size_t writableSize() { return buffer_.size() - writer_index_; }
  size_t discardableSize() { return reader_index_; }
  bool readable() { return readableSize() > 0; }
  bool writable() { return writableSize() > 0; }

  const char* findCRLF() const {
    const char* pos = std::search(readerZone(), writerZone(), CRLF, CRLF + 2);
    return (pos == writerZone()) ? nullptr : pos;
  }

  const char* findCRLF(const char* begin) const {
    assert(readerZone() <= begin);
    assert(begin <= writerZone());
    const char* pos = std::search(begin, writerZone(), CRLF, CRLF + 2);
    return (pos == writerZone()) ? nullptr : pos;
  }

  const char* findEOL() const {
    const char* pos = std::find(readerZone(), writerZone(), '\n');
    return (pos == writerZone()) ? nullptr : pos;
  }

  const char* findEOL(const char* begin) const {
    assert(readerZone() <= begin);
    assert(begin <= writerZone());
    const char* pos = std::find(readerZone(), writerZone(), '\n');
    return (pos == writerZone()) ? nullptr : pos;
  }

  void write(const char* data, size_t len) {
    this->reserve(len);
    std::copy(data, data + len, writerZone());
    readerIndexShift(len);
  }
  void write(const void* data, size_t len) {
    write(static_cast<const char*>(data), len);
  }
  void writeInt16(int16_t x) { write(&x, sizeof(x)); }
  void writeInt32(int32_t x) { write(&x, sizeof(x)); }
  size_t recvFrom(int fd, int* saved_errno = nullptr);

  const char* peek() const { return data() + reader_index_; }
  int16_t peekInt16() {
    assert(readableSize() >= sizeof(int16_t));
    int16_t result = 0;
    ::memcpy(&result, peek(), sizeof(int16_t));
    return result;
  }
  int32_t peekInt32() {
    assert(readableSize() >= sizeof(int32_t));
    int32_t result = 0;
    ::memcpy(&result, peek(), sizeof(int32_t));
    return result;
  }

  size_t read(char* buf, std::size_t count);
  int16_t readInt16() {
    int16_t result = peekInt16();
    readerIndexShift(sizeof(int16_t));
    return result;
  }
  int32_t readInt32() {
    int32_t result = peekInt32();
    readerIndexShift(sizeof(int32_t));
    return result;
  }
  size_t sendTo(int fd, int* saved_errno);

  void skip(size_t len) {
    assert(len <= readableSize());
    readerIndexShift(len);
  }
  void skipAll() {
    reader_index_ = 0;
    writer_index_ = 0;
  }

  void readerIndexShift(size_t len) {
    assert(len <= readableSize());
    if (len < readableSize()) {
      reader_index_ += len;
    } else {
      reader_index_ = 0;
      writer_index_ = 0;
    }
  }
  void writerIndexShift(size_t len) {
    assert(len <= writableSize());
    writer_index_ += len;
  }
  size_t readerIndex() const { return reader_index_; }
  size_t writerIndex() const { return writer_index_; }
  char* readerZone() { return data() + reader_index_; }
  char* writerZone() { return data() + writer_index_; }
  const char* readerZone() const { return data() + reader_index_; }
  const char* writerZone() const { return data() + writer_index_; }
  char* begin() { return data() + reader_index_; }
  char* end() { return data() + writer_index_; }

  size_t capacity() const { return buffer_.capacity(); }

  void shrink(size_t len) {
    if (reader_index_ != 0) {
      int readable_size = readableSize();
      std::copy(data() + reader_index_, data() + writer_index_, data());
      reader_index_ = 0;
      writer_index_ = reader_index_ + readable_size;
    }
    buffer_.resize(readableSize() + len);
    buffer_.shrink_to_fit();
  }

  void reserve(size_t len) {
    if (writableSize() < len) {
      assign(len);
    }
    assert(writableSize() >= len);
  }

  void assign(size_t len) {
    if (writableSize() + discardableSize() < len) {
      buffer_.resize(writer_index_ + len);
    } else {
      int readable_size = readableSize();
      std::copy(data() + reader_index_, data() + writer_index_, data());
      reader_index_ = 0;
      writer_index_ = reader_index_ + readable_size;
    }
  }

  void reset() {
    reader_index_ = 0;
    writer_index_ = 0;
  }

  void clear() { reset(); }

  ChannelBuffer& operator+=(const std::string& s) {
    this->write(s.c_str(), s.size());
    return *this;
  }

  ChannelBuffer& operator=(const std::string& s) {
    this->reset();
    this->write(s.c_str(), s.size());
    return *this;
  }

 private:
  char* data() { return buffer_.data(); }
  const char* data() const { return buffer_.data(); }

 private:
  std::vector<char> buffer_;
  size_t reader_index_;
  size_t writer_index_;
};

class FixedBuffer {};

#endif