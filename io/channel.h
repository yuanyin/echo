#ifndef NET_CHANNEL_H_
#define NET_CHANNEL_H_

#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <unistd.h>

#include <cassert>
#include <cstring>
#include <functional>
#include <map>
#include <memory>
#include <mutex>

class Channel {
 public:
  typedef std::function<void()> HandleFunc;
  static const uint32_t kNoneEvent;
  static const uint32_t kReadEvent;
  static const uint32_t kWriteEvent;

 public:
  Channel(int fd) : fd_(fd), index_(0) {}
  void process();

  uint32_t getExternalEvent() { return external_event_; }
  uint32_t getEvent() { return event_; }

  int sock() const { return fd_; }
  int fd() const { return fd_; }

  bool compareAndUpdateEvent() {
    if (event_ == internal_event_) {
      return true;
    }

    internal_event_ = event_;
    return false;
  }

  bool hasNoneEvent() { return event_ == kNoneEvent; }
  bool hasReadEvent() { return event_ & kReadEvent; }
  bool hasWriteEvent() { return event_ & kWriteEvent; }

  int getIndex() const { return index_; }

  void setIndex(int index) { index_ = index; }

  void setExternalEvent(uint32_t event) { external_event_ = event; }

  void setEvent(uint32_t event) { event_ = event; }

  void setReadHandler(HandleFunc handler) {
    read_handler_ = std::move(handler);
  }

  void setWriteHandler(HandleFunc handler) {
    write_handler_ = std::move(handler);
  }

  void setCloseHandler(HandleFunc handler) {
    close_handler_ = std::move(handler);
  }

  void setErrorHandler(HandleFunc handler) {
    error_handler_ = std::move(handler);
  }

  void handleEvent();

 private:
  const int fd_;
  int index_;

  uint32_t external_event_;
  uint32_t event_;
  uint32_t internal_event_;

  std::function<void()> read_handler_;
  // EPOLLOUT
  std::function<void()> write_handler_;
  std::function<void()> error_handler_;
  // EPOLLIN | EPOLLRDHUP
  std::function<void()> close_handler_;
};

typedef std::vector<Channel*> ChannelList;
typedef std::shared_ptr<Channel> ChannelPtr;

#endif