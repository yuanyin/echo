#include <windows.h>

#include <cassert>
#include <limits>

#include "io.h"

std::string GetWindowsErrorMessage(DWORD error_code) {
  std::string message;
  char* error_text = nullptr;
  // Use MBCS version of FormatMessage to match return value.
  size_t error_text_size = ::FormatMessageA(
      FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER |
          FORMAT_MESSAGE_IGNORE_INSERTS,
      nullptr, error_code, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
      reinterpret_cast<char*>(&error_text), 0, nullptr);
  if (!error_text) {
    return message;
  }
  message.assign(error_text, error_text_size);
  ::LocalFree(error_text);
  return message;
}

Status WindowsError(const std::string& context, DWORD error_code) {
  if (error_code == ERROR_FILE_NOT_FOUND || error_code == ERROR_PATH_NOT_FOUND)
    return Status::NotFound(context, GetWindowsErrorMessage(error_code));
  return Status::IOError(context, GetWindowsErrorMessage(error_code));
}

class ScopedHandle {
 public:
  ScopedHandle(HANDLE handle) : handle_(handle) {}
  ScopedHandle(const ScopedHandle&) = delete;
  ScopedHandle(ScopedHandle&& other) noexcept : handle_(other.release()) {}
  ~ScopedHandle() { close(); }

  ScopedHandle& operator=(const ScopedHandle&) = delete;

  ScopedHandle& operator=(ScopedHandle&& rhs) noexcept {
    if (this != &rhs) handle_ = rhs.release();
    return *this;
  }

  bool close() {
    if (!is_valid()) {
      return true;
    }
    HANDLE h = handle_;
    handle_ = INVALID_HANDLE_VALUE;
    return ::CloseHandle(h);
  }

  bool is_valid() const {
    return handle_ != INVALID_HANDLE_VALUE && handle_ != nullptr;
  }

  HANDLE get() const { return handle_; }

  HANDLE release() {
    HANDLE h = handle_;
    handle_ = INVALID_HANDLE_VALUE;
    return h;
  }

 private:
  HANDLE handle_;
};

class WindowsFileReaderImpl : public FileReaderImpl {
 public:
  WindowsFileReaderImpl(ScopedHandle handle) : handle_(std::move(handle)) {}

  ~WindowsFileReaderImpl() {}

  Status read(char* buf, size_t len) override {
    DWORD ret;
    // assert(len <= std::numeric_limits<DWORD>::max());
    if (!::ReadFile(handle_.get(), buf, static_cast<DWORD>(len), &ret,
                    nullptr)) {
      // LOG(ERROR)
      return WindowsError("filename", ::GetLastError());
    }
    return Status::OK();
  }

 private:
  const ScopedHandle handle_;
};

std::unique_ptr<FileReaderImpl> CreateFileReaderImpl(
    std::filesystem::path file_path) {
  DWORD desired_access = GENERIC_READ;
  DWORD share_mode = FILE_SHARE_READ;

  auto filename = file_path.generic_string();
  ScopedHandle handle = ::CreateFileA(
      filename.c_str(), desired_access, share_mode,
      /*lpSecurityAttributes=*/nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL,
      /*hTemplateFile=*/nullptr);
  if (!handle.is_valid()) {
    // LOG(ERROR)
  }

  return std::make_unique<WindowsFileReaderImpl>(std::move(handle));
}