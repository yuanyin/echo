#include "channel.h"

#include "util/logging.h"

const uint32_t Channel::kNoneEvent = 0;
const uint32_t Channel::kReadEvent = EPOLLIN | EPOLLPRI;
const uint32_t Channel::kWriteEvent = EPOLLOUT;

void Channel::handleEvent() {
  // EPOLLIN: 表示对应的文件描述符可以读（包括对端SOCKET正常关闭）；
  // EPOLLOUT: 表示对应的文件描述符可以写;
  // EPOLLPRI: 表示对应的文件描述符有紧急的数据可读;
  // EPOLLERR: 表示对应的文件描述符发生错误;
  // EPOLLHUP: 表示对应的文件描述符被挂断;
  // EPOLLET: 将EPOLL设为边缘触发;
  // EPOLLONESHOT: 监听一次，如果要继续监听socket的话，需要再次加入到EPOLL

  if ((external_event_ & EPOLLHUP) && !(external_event_ & EPOLLIN)) {
    if (close_handler_) {
      close_handler_();
    }
  }

  if (external_event_ & EPOLLERR) {
    if (error_handler_) {
      error_handler_();
    }
  }

  if (external_event_ & (EPOLLIN | EPOLLPRI | EPOLLRDHUP)) {
    if (read_handler_) {
      read_handler_();
    }
  }

  if (external_event_ & EPOLLOUT) {
    if (write_handler_) {
      write_handler_();
    }
  }

  if (external_event_ & EPOLLRDHUP) {
    if (close_handler_) {
      close_handler_();
    }
  }
}