#ifndef SELECTOR_H_
#define SELECTOR_H_

#include <sys/epoll.h>

#include "channel.h"

class Selector {
 public:
  typedef std::vector<struct epoll_event> EventList;
  typedef std::vector<Channel*> ChannelList;
  typedef std::unordered_map<int, Channel*> ChannelMap;

 public:
  Selector();
  ~Selector();

  void select(ChannelList*);

  void insertChannel(Channel* chann);

  void updateChannel(Channel* chann);

  void removeChannel(Channel* chann);

  bool hasChannel(Channel* chann) const;

 private:
  void update(int op, Channel* chann);

 private:
  static const int kInitEventListSize = 1024;

  int epollfd_;
  ChannelMap channel_store_;
  EventList event_list_;
};

#endif