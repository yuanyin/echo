#include <fcntl.h>
#include <unistd.h>

#include "file.h"

class PosixFileInputStreamImpl : public FileInputStreamImpl {
 public:
  PosixFileInputStreamImpl(std::filesystem::path file_path) {
    std::clog << __FILE__ << ":" << __LINE__ << ":" << __func__ << std::endl;

    this->fd_ = ::open(file_path.c_str(), O_RDWR);
    std::clog << file_path.c_str() << " fd = " << fd_ << std::endl;
    if (this->fd_ < 0) {
      perror("open");
      std::abort();
    }
  }

  ~PosixFileInputStreamImpl() { ::close(fd_); }

  ssize_t read(char* buf, size_t len) override {
    std::clog << __FILE__ << ":" << __LINE__ << ":" << __func__ << std::endl;
    std::clog << " fd = " << fd_ << std::endl;
    ssize_t ret = ::read(fd_, buf, len);
    std::clog << ret << " bytes" << std::endl;
    return ret;
  }

 private:
  int fd_;
};

std::unique_ptr<FileInputStreamImpl> CreateFileInputStream(
    std::filesystem::path file_path) {
  return std::make_unique<PosixFileInputStreamImpl>(file_path);
}