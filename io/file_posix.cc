#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

#include <atomic>

#include "io.h"

// Common flags defined for all posix open operations
#if defined(HAVE_O_CLOEXEC)
constexpr const int kOpenBaseFlags = O_CLOEXEC;
#else
constexpr const int kOpenBaseFlags = 0;
#endif  // defined(HAVE_O_CLOEXEC)

Status PosixError(const std::string& context, int error_number) {
  if (error_number == ENOENT) {
    return Status::NotFound(context, std::strerror(error_number));
  } else {
    return Status::IOError(context, std::strerror(error_number));
  }
}

// Helper class to limit resource usage to avoid exhaustion.
// Currently used to limit read-only file descriptors and mmap file usage
// so that we do not run out of file descriptors or virtual memory, or run into
// kernel performance problems for very large databases.
class Limiter {
 public:
  // Limit maximum number of resources to |max_acquires|.
  Limiter(int max_acquires)
      :
#if !defined(NDEBUG)
        max_acquires_(max_acquires),
#endif  // !defined(NDEBUG)
        acquires_allowed_(max_acquires) {
    assert(max_acquires >= 0);
  }

  Limiter(const Limiter&) = delete;
  Limiter operator=(const Limiter&) = delete;

  // If another resource is available, acquire it and return true.
  // Else return false.
  bool Acquire() {
    int old_acquires_allowed =
        acquires_allowed_.fetch_sub(1, std::memory_order_relaxed);

    if (old_acquires_allowed > 0) return true;

    int pre_increment_acquires_allowed =
        acquires_allowed_.fetch_add(1, std::memory_order_relaxed);

    // Silence compiler warnings about unused arguments when NDEBUG is defined.
    (void)pre_increment_acquires_allowed;
    // If the check below fails, Release() was called more times than acquire.
    assert(pre_increment_acquires_allowed < max_acquires_);

    return false;
  }

  // Release a resource acquired by a previous call to Acquire() that returned
  // true.
  void Release() {
    int old_acquires_allowed =
        acquires_allowed_.fetch_add(1, std::memory_order_relaxed);

    // Silence compiler warnings about unused arguments when NDEBUG is defined.
    (void)old_acquires_allowed;
    // If the check below fails, Release() was called more times than acquire.
    assert(old_acquires_allowed < max_acquires_);
  }

 private:
#if !defined(NDEBUG)
  // Catches an excessive number of Release() calls.
  const int max_acquires_;
#endif  // !defined(NDEBUG)

  // The number of available resources.
  //
  // This is a counter and is not tied to the invariants of any other class, so
  // it can be operated on safely using std::memory_order_relaxed.
  std::atomic<int> acquires_allowed_;
};

class PosixFileWriterImpl : public FileWriterImpl {
 public:
  PosixFileWriterImpl(std::filesystem::path filepath, int fd)
      : filepath_(std::move(filepath)), fd_(fd) {}

  ~PosixFileWriterImpl() { this->close(); }

  Status write(const char* buf, size_t len) override {
    while (len > 0) {
      ssize_t write_result = ::write(fd_, buf, len);
      if (write_result < 0) {
        if (errno == EINTR) {
          continue;
        }
        return PosixError(filepath_.generic_string(), errno);
      }
      buf += write_result;
      len -= write_result;
    }

    return Status::OK();
  }

  void flush() {}

  Status close() {
    Status status;
    if (fd_ >= 0) {
      const int close_result = ::close(fd_);
      if (close_result < 0) {
        status = PosixError(filepath_.generic_string(), errno);
      }
    }
    return status;
  }

 private:
  std::filesystem::path filepath_;
  int fd_;
};

class PosixFileReaderImpl : public FileReaderImpl {
 public:
  PosixFileReaderImpl(std::filesystem::path filepath, int fd)
      : filepath_(filepath), fd_(fd) {}

  ~PosixFileReaderImpl() { ::close(fd_); }

  Status read(char* buf, size_t len) override {
    Status status;
    while (true) {
      ssize_t read_size = ::read(fd_, buf, len);
      if (read_size < 0) {
        if (errno == EINTR) {
          continue;
        }
        status = PosixError("filename", errno);
      }

      break;
    }

    return status;
  }

  Status skip(size_t len) {
    if (::lseek(fd_, len, SEEK_CUR) == static_cast<off_t>(-1)) {
      return PosixError("filename", errno);
    }
    return Status::OK();
  }

 private:
  std::filesystem::path filepath_;
  int fd_;
};

std::unique_ptr<FileWriterImpl> CreateFileWriterImpl(
    const std::filesystem::path& file_path) {
  int fd = ::open(file_path.c_str(), O_CREAT | O_TRUNC | O_WRONLY, 0644);
  if (fd < 0) {
    // LOG(Error)
  }
  return std::make_unique<PosixFileWriterImpl>(file_path, fd);
}

std::unique_ptr<FileReaderImpl> CreateFileReaderImpl(
    const std::filesystem::path& file_path) {
  int fd = ::open(file_path.c_str(), O_RDONLY);
  if (fd < 0) {
    // LOG(Error)
  }
  return std::make_unique<PosixFileReaderImpl>(file_path, fd);
}