#include <iostream>
#include <string>

#include "io.h"

int main() {
  std::filesystem::path file_path = "io.h";
  size_t file_size = std::filesystem::file_size(file_path);

  std::string s(file_size, 'a');
  char *buf = &(*s.begin());
  FileReader r(file_path);

  r.read(buf, file_size);

  std::cout << s << std::endl;
}