#include "selector.h"

#include "util/logging.h"

namespace {
const int STATE_NEW = 0;
const int STATE_ADD = 1;
const int STATE_DEL = 2;
}  // namespace

Selector::Selector()
    : epollfd_(::epoll_create1(EPOLL_CLOEXEC)),
      event_list_(kInitEventListSize) {}

Selector::~Selector() { ::close(epollfd_); }

void Selector::select(ChannelList* active_channel_list) {
  LOG(TRACE) << "Total fd number is " << channel_store_.size();

  int num_events = ::epoll_wait(epollfd_, &(*event_list_.begin()),
                                static_cast<int>(event_list_.size()), -1);

  if (num_events < 0) {
    if (errno == EINTR) {
      LOG(ERROR) << "Epoll wait: return value < 1";
    }
    return;
  } else if (num_events == 0) {
    LOG(TRACE) << "Epoll wait: nothing happened";
    return;
  }

  for (int i = 0; i < num_events; i++) {
    Channel* channel = static_cast<Channel*>(event_list_[i].data.ptr);
    int fd = channel->fd();

    assert(channel_store_.count(fd));
    channel->setExternalEvent(event_list_[i].events);
    active_channel_list->push_back(channel);
  }
}

void Selector::insertChannel(Channel* chann) {
  int fd = chann->fd();
  channel_store_[fd] = chann;
  update(EPOLL_CTL_ADD, chann);
}

void Selector::updateChannel(Channel* chann) {
  const int fd = chann->fd();
  const int index = chann->getIndex();

  if (index == STATE_NEW || index == STATE_DEL) {
    if (index == STATE_NEW) {
      assert(!channel_store_.count(fd));
      channel_store_[fd] = chann;
    } else {
      assert(channel_store_.count(fd));
      assert(channel_store_[fd] == chann);
    }

    chann->setIndex(STATE_ADD);
    update(EPOLL_CTL_ADD, chann);
  } else {
    assert(channel_store_.count(fd));
    assert(channel_store_[fd] == chann);
    assert(index == STATE_ADD);

    if (chann->hasNoneEvent()) {
      update(EPOLL_CTL_DEL, chann);
      chann->setIndex(STATE_DEL);
    } else {
      update(EPOLL_CTL_MOD, chann);
    }
  }
}

void Selector::removeChannel(Channel* chann) {
  int fd = chann->fd();
  assert(channel_store_.count(fd));
  assert(channel_store_[fd] == chann);
  channel_store_.erase(fd);

  update(EPOLL_CTL_DEL, chann);
}

bool Selector::hasChannel(Channel* chann) const {
  int fd = chann->fd();
  return channel_store_.count(fd);
}

const char* operationToString(int op) {
  switch (op) {
    case EPOLL_CTL_ADD:
      return "EPOLL_CTL_ADD";
    case EPOLL_CTL_DEL:
      return "EPOLL_CTL_DEL";
    case EPOLL_CTL_MOD:
      return "EPOLL_CTL_MOD";
    default:
      return "Unknown Operation";
  }
}

void Selector::update(int op, Channel* chann) {
  struct epoll_event event;
  bzero(&event, sizeof event);
  event.events = chann->getEvent();
  event.data.ptr = chann;
  int fd = chann->sock();

  if (::epoll_ctl(epollfd_, op, fd, &event) < 0) {
    if (op == EPOLL_CTL_DEL) {
      LOG(ERROR) << "epoll_ctl failed: op = " << operationToString(op)
                 << " fd =" << fd;
    } else {
      LOG(FATAL) << "epoll_ctl fatal: op =" << operationToString(op)
                 << " fd =" << fd;
    }
  }
}