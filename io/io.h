#ifndef IO_H_
#define IO_H_

#include <cstdint>
#include <filesystem>
#include <string>

#include "util/status.h"

class Reader {
 public:
  Reader() = default;
  virtual ~Reader() = default;

  virtual Status read(char *buf, size_t len) const = 0;
};

class Writer {
 public:
  Writer() = default;
  virtual ~Writer() = default;

  virtual Status write(const char *buf, size_t len) = 0;
};

class Closer {
 public:
  Closer() = default;
  virtual ~Closer() = default;

  virtual Status close() = 0;
};

class StringReader {
 public:
  virtual Status readString(std::string &s) const = 0;
};

class StringWriter {
 public:
  virtual Status writeString(const std::string &s) = 0;
};

class File {
 public:
  File(std::string filename);
  File(const char *filename);
  File(std::filesystem::path path);
};

class FileWriterImpl {
 public:
  FileWriterImpl() = default;
  virtual ~FileWriterImpl() = default;
  virtual Status write(const char *buf, size_t len) = 0;
};

class FileReaderImpl {
 public:
  FileReaderImpl() = default;
  virtual ~FileReaderImpl() = default;
  virtual Status read(char *buf, size_t len) = 0;
};

std::unique_ptr<FileReaderImpl> CreateFileReaderImpl(
    const std::filesystem::path &);

std::unique_ptr<FileWriterImpl> CreateFileWriterImpl(
    const std::filesystem::path &);

class FileWriter : public Writer {
 public:
  FileWriter(std::filesystem::path file_path)
      : impl_(CreateFileWriterImpl(file_path)) {}
  ~FileWriter() = default;
  Status write(const char *buf, size_t len) override {
    return impl_->write(buf, len);
  }

 protected:
  std::unique_ptr<FileWriterImpl> impl_;
};

class FileReader : public Reader {
 public:
  FileReader(std::filesystem::path file_path)
      : impl_(CreateFileReaderImpl(file_path)) {}
  ~FileReader() = default;

  Status read(char *buf, size_t len) const override {
    Status ret = impl_->read(buf, len);
    return ret;
  }

 private:
  std::unique_ptr<FileReaderImpl> impl_;
};

#endif