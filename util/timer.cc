// Author: Yuanyin Zhang
// Email: isyuanyin@gmail.com

#include "timer.h"

#include "util/logging.h"

/// TimeType Implementation
///
const long TimeType::NANO = 1000000000;

TimeType TimeType::nowTime() {
  timespec now_time;
  if (clock_gettime(CLOCK_ID, &now_time) == -1) {
    perror("clock gettime");
    LOG(ERROR) << __func__;
    std::exit(EXIT_FAILURE);
  }
  return TimeType(now_time);
}

TimeType TimeType::absTime(TimeType rel) { return rel + nowTime(); }
TimeType TimeType::relTime(TimeType abs) { return abs - nowTime(); }

/// TimeEvent Implementation
///
TimeType TimeEvent::TIME_SLOT(5);
int TimeEvent::kTimerFd = timerfd_create(CLOCK_ID, TFD_CLOEXEC | TFD_NONBLOCK);

void TimeEvent::asyncThread() {
  int epfd = epoll_create1(EPOLL_CLOEXEC);
  int tfd = timerfd_create(CLOCK_ID, TFD_CLOEXEC | TFD_NONBLOCK);

  if (epfd == -1) {
    perror("epoll_create1");
  }

  epoll_event event;
  event.data.fd = tfd;
  event.events = EPOLLIN | EPOLLET;
  epoll_ctl(epfd, EPOLL_CTL_ADD, tfd, &event);

  while (true) {
    int num = epoll_wait(epfd, &event, 1, 0);
    if (num == 1) {
      if (event.events & EPOLLIN) {
        Timer::tick();
      }
    }
  }
}

void TimeEvent::asyncExecute() {
  std::thread t(asyncThread);
  t.detach();
}

void TimeEvent::handleTimeEvent() { Timer::tick(); }
void TimeEvent::startTimeEvent(int epfd, const TimeType &value,
                               const TimeType &interval) {
  epoll_event event;
  event.data.fd = kTimerFd;
  event.events = EPOLLIN | EPOLLET;

  int ret = epoll_ctl(epfd, EPOLL_CTL_ADD, kTimerFd, &event);
  if (ret == -1) {
    perror("epoll_ctl");
    LOG(ERROR) << __func__;
    std::exit(EXIT_FAILURE);
  }

  setTimeout(value, interval);
}

void TimeEvent::setTimeout(const TimeType &value, const TimeType &interval) {
  itimerspec its;
  its.it_value.tv_sec = value.second();
  its.it_value.tv_nsec = value.nanosecond();
  its.it_interval.tv_sec = interval.second();
  its.it_interval.tv_nsec = interval.nanosecond();

  int ret = timerfd_settime(kTimerFd, 0, &its, NULL);

  if (ret == -1) {
    perror("timefd_settime");
  }
}

void TimeEvent::setInternal(const TimeType &interval) {
  setTimeout(interval, interval);
}

#ifdef DEBUG
TimerPool *TimerPool::GetPool(TimerPool *set_pool) {
  static auto pool = dynamic_cast<TimerPool *>(new TimerHeap());
  if (set_pool) {
    pool->clear();
    pool = set_pool;
  }
  return pool;
}
#else
TimerPool *TimerPool::GetPool() {
  static auto pool = new TimerHeap();
  return dynamic_cast<TimerPool *>(pool);
}
#endif

TimerPool::~TimerPool() {}

/// TimerList Implementation
///
TimerList::TimerListNode::TimerListNode(TimeType exp, Action cb)
    : TimerNode(exp, cb), ListNode() {}

TimeType TimerList::TimerListNode::nodeToExpiredTime(ListNode *node) {
  return (dynamic_cast<TimerNode *>(node))->getExpiredTime();
}

int TimerList::TimerListNode::compareExpiredTime(ListNode *n1, ListNode *n2) {
  auto t1 = nodeToExpiredTime(n1);
  auto t2 = nodeToExpiredTime(n2);
  if (t1 < t2) {
    return -1;
  } else if (t1 > t2) {
    return 1;
  }
  return 0;
}

bool TimerList::addTimer(TimerNode *timer_node) {
  std::unique_lock<std::mutex> lock(mutex_);

  auto node = dynamic_cast<ListNode *>(timer_node);
  auto curr = list_head_->next;
  while (curr != list_head_) {
    if (TimerListNode::compareExpiredTime(node, curr) <= 0) {
      break;
    }
    curr = curr->next;
  }

  if (ListNode::insertNode(curr, node)) {
    ++size_;
    return true;
  }

  return false;
}

bool TimerList::delTimer(TimerNode *timer_node) {
  std::unique_lock<std::mutex> lock(mutex_);

  auto node = dynamic_cast<ListNode *>(timer_node);
  if (node == nullptr) {
    // LOG(WARN)
    return false;
  }
  if (ListNode::removeNode(node)) {
    --size_;
    return true;
  }

  return false;
}

TimerNode *TimerList::topTimer() {
  std::unique_lock<std::mutex> lock(mutex_);

  if (list_head_->next == list_head_) return nullptr;
  return dynamic_cast<TimerNode *>(list_head_->next);
}

bool TimerList::isInPool(TimerNode *timer_node) {
  auto node = dynamic_cast<ListNode *>(timer_node);
  if (node == nullptr) return false;

  std::unique_lock<std::mutex> lock(mutex_);

  if (node->prev == node && node->prev == node) {
    return false;
  } else if (node->prev && node->next) {
    return true;
  } else {
    // LOG(ERROR)
    LOG(ERROR) << __func__;
    std::exit(EXIT_FAILURE);
  }
}

void TimerList::clear() {
  while (!this->empty()) {
    this->popTimer();
  }
}

/// TimerHeap Implementation
///
TimeType TimerHeap::TimerHeapNode::nodeToExpiredTime(HeapNode *node) {
  return (dynamic_cast<TimerNode *>(node))->getExpiredTime();
}

int TimerHeap::TimerHeapNode::compareExpiredTime(HeapNode *n1, HeapNode *n2) {
  auto t1 = nodeToExpiredTime(n1);
  auto t2 = nodeToExpiredTime(n2);
  if (t1 < t2) {
    return -1;
  } else if (t1 > t2) {
    return 1;
  }

  return 0;
}

std::unique_ptr<TimerNode> TimerHeap::generateNode(TimeType exp, Action cb) {
  return std::make_unique<TimerHeapNode>(exp, cb);
}

bool TimerHeap::addTimer(TimerNode *timer_node) {
  std::unique_lock<std::mutex> lock(mutex_);

  auto node = dynamic_cast<HeapNode *>(timer_node);
  if (!heap_top_) {
    heap_top_ = node;
    heap_top_->prev = heap_top_->left = heap_top_->right = nullptr;
    heap_top_->high = 1;

    size_ = 1;
    return true;
  }

  auto curr = heap_top_;

  int reason = 0;

  while (curr) {
    if (TimerHeapNode::compareExpiredTime(node, curr) <= 0) {
      reason = 0;
      break;
    }

    if (!curr->left || !curr->right) {  //  不能再往下走了
      reason = 1;
      break;
    }

    if (curr->left->high <= curr->right->high) {
      curr = curr->left;
    } else {
      curr = curr->right;
    }
  }

  if (reason == 1) {
    if (HeapNode::appendNode(curr, node)) {
      ++size_;
      return true;
    }

  } else {
    // 在内部插入
    if (HeapNode::insertNode(curr, node)) {
      ++size_;

      if (curr == heap_top_) {
        heap_top_ = node;
      }
      return true;
    }
  }

  return false;
}

bool TimerHeap::delTimer(TimerNode *timer_node) {
  std::unique_lock<std::mutex> lock(mutex_);

  auto node = dynamic_cast<HeapNode *>(timer_node);

  if (node == heap_top_) {
    if (node->right == nullptr) {
      heap_top_ = node->left;
    } else if (node->left == nullptr) {
      heap_top_ = node->right;
    } else if (TimerHeapNode::compareExpiredTime(node->left, node->right) <=
               0) {
      heap_top_ = node->left;
    } else {
      heap_top_ = node->right;
    }
  }

  if (HeapNode::removeNode(node, [](HeapNode *l, HeapNode *r) {
        if (r == nullptr) {
          return HeapNode::NODE_LEFT;
        }
        if (l == nullptr) {
          return HeapNode::NODE_RIGHT;
        }
        /// 数值小的才能上去
        if (TimerHeapNode::compareExpiredTime(l, r) <= 0) {
          return HeapNode::NODE_LEFT;
        } else {
          return HeapNode::NODE_RIGHT;
        }
      })) {
    // 删除成功
    --size_;
    return true;
  }

  if (heap_top_ != nullptr) {
    // LOG(ERROR)
  }
  return false;
}

bool TimerHeap::updateTimer(TimerNode *timer_node, TimeType expire) {
  std::unique_lock<std::mutex> lock(mutex_);

  auto old_expire = timer_node->getExpiredTime();
  if (expire == old_expire) {
    return true;
  }

  timer_node->setExpriedTime(expire);
  auto node = dynamic_cast<HeapNode *>(timer_node);

  if (expire < old_expire) {
    // curr 在 node 上面
    auto curr = node->prev;
    while (curr) {
      if (TimerHeapNode::compareExpiredTime(node, curr) <= 0) {
        break;
      }
      HeapNode::upNode(node);
      curr = node->prev;
    }

    if (curr == nullptr) {
      heap_top_ = node;
    }

  } else {
    if (!node->left && !node->right) {
      return true;
    }

    if (node->left && TimerHeapNode::compareExpiredTime(node, node->left) > 0) {
      if (node == heap_top_) {
        heap_top_ = node->left;
      }
      HeapNode::upNode(node->left);
    } else if (node->right &&
               TimerHeapNode::compareExpiredTime(node, node->right) > 0) {
      if (node == heap_top_) {
        heap_top_ = node->right;
      }
      HeapNode::upNode(node->right);
    }

    while (node->left || node->right) {
      if (node->left &&
          TimerHeapNode::compareExpiredTime(node, node->left) > 0) {
        HeapNode::upNode(node->left);
      } else if (node->right &&
                 TimerHeapNode::compareExpiredTime(node, node->right) > 0) {
        HeapNode::upNode(node->right);
      } else {
        break;
      }
    }
  }

  return true;
}

TimerNode *TimerHeap::topTimer() {
  std::unique_lock<std::mutex> lock(mutex_);
  if (!heap_top_) return nullptr;
  return dynamic_cast<TimerNode *>(heap_top_);
}

bool TimerHeap::isInPool(TimerNode *timer_node) {
  auto node = dynamic_cast<HeapNode *>(timer_node);
  if (node == nullptr) return false;

  std::unique_lock<std::mutex> lock(mutex_);

  if (node->left == node && node->right == node && node->prev == node) {
    return false;
  } else if (node->left != node && node->right != node && node->prev != node) {
    return true;
  } else {
    LOG(ERROR) << __func__;
    std::exit(EXIT_FAILURE);
  }
}

void TimerHeap::clear() {
  while (!this->empty()) {
    this->popTimer();
  }
}

/// Timer Implementation
///
TimerImpl::TimerImpl(TimeType exp, Action action)
    : node_(TimerPool::GetPool()->generateNode(exp, action)) {}

void TimerImpl::tick() {
  auto pool = TimerPool::GetPool();
  auto now_time = TimeType::nowTime();

  while (!pool->empty()) {
    TimerNode *timer_node = pool->topTimer();
    auto node_time = timer_node->getExpiredTime();
    if (node_time > now_time) break;

    if (timer_node && timer_node->action) {
      (timer_node->action)();
    }
    pool->popTimer();
  }
}

Timer::Timer(TimeType exp, Action action, bool start)
    : timer_impl_(std::make_unique<TimerImpl>(exp, action)) {
  if (start) {
    timer_impl_->start();
  }
}