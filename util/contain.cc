
#include "contain.h"

#include "util/logging.h"

/// ListNode Implementation
///
bool ListNode::insertNode(ListNode *curr, ListNode *node) {
  if (curr == nullptr || node == nullptr) return false;
  node->prev = curr->prev;
  node->next = curr;
  curr->prev->next = node;
  curr->prev = node;
  return true;
}

bool ListNode::removeNode(ListNode *node) {
  if (node == nullptr) return false;
  node->prev->next = node->next;
  node->next->prev = node->prev;
  node->prev = node->next = node;

  return true;
}

/// HeapNode Implementation
///
bool HeapNode::upNode(HeapNode *node, NODE_DIV div) {
  if (node == nullptr || node->prev == nullptr) return false;

  auto curr = node->prev;
  /// 确定方向
  if (div == NODE_NONE) {
    if (curr->left == node) {
      div = NODE_LEFT;
    } else if (curr->right == node) {
      div = NODE_RIGHT;
    }
  }

  /// 处理外部对两者的指向：curr上node下 -> curr下node上
  // 上面的指向
  if (curr->prev) {
    if (curr->prev->left == curr) {
      curr->prev->left = node;
    } else {
      curr->prev->right = node;
    }
  }

  if (node->left) {
    node->left->prev = curr;
  }
  if (node->right) {
    node->right->prev = curr;
  }

  if (div == NODE_LEFT) {
    if (curr->right) curr->right->prev = node;
  } else if (div == NODE_RIGHT) {
    if (curr->left) curr->left->prev = node;
  } else {
    LOG(ERROR) << __func__;
  }

  /// 交换两者对外部的指向和数值
  std::swap(node->high, curr->high);
  std::swap(node->prev, curr->prev);
  std::swap(node->left, curr->left);
  std::swap(node->right, curr->right);

  /// 处理两者的相互指向
  curr->prev = node;

  if (div == NODE_LEFT) {
    node->left = curr;
  } else if (div == NODE_RIGHT) {
    node->right = curr;
  } else {
    LOG(ERROR) << __func__;
  }

  return true;
}

bool HeapNode::downNode(HeapNode *node, NODE_DIV div) {
  HeapNode *curr = nullptr;

  /// 必须指定方向才能执行
  if (div == NODE_LEFT) {
    curr = node->left;
  } else if (div == NODE_RIGHT) {
    curr = node->right;
  } else {
    return false;
  }

  return upNode(curr, div);
}

int HeapNode::recursiveUpNode(HeapNode *node, const DivSelect &select) {
  if (node == nullptr || node->empty()) return -1;
  auto div = select(node->left, node->right);

  HeapNode *curr = nullptr;
  int ret = -1;

  if (div == NODE_LEFT) {
    curr = node->left;
    if (node->right) ret = std::max(ret, node->right->high + 1);

  } else if (div == NODE_RIGHT) {
    curr = node->right;
    if (node->left) ret = std::max(ret, node->left->high + 1);

  } else {
    // LOG(ERROR)
  }

  if (curr) {
    if (!upNode(curr, div)) {
      LOG(ERROR) << __func__;
      std::exit(EXIT_FAILURE);
    }
    ret = std::max(recursiveUpNode(node, select) + 1, ret);
    curr->high = ret;
    return ret;

  } else {
    /// 到堆的底部了

    if (node->prev) {
      if (node->prev->left == node) {
        node->prev->left = nullptr;
      } else if (node->prev->right == node) {
        node->prev->right = nullptr;
      } else {
        // LOG(ERROR)
        return -1;
      }
    }

    node->reset();
    return 0;
  }
}

/// 如果插入新结点需要指定node->prev
HeapNode::NODE_DIV HeapNode::recursiveDownNode(HeapNode *node,
                                               const DivSelect &select) {
  if (node == nullptr || node->empty()) return NODE_NONE;

  auto div = select(node->left, node->right);

  HeapNode *curr = nullptr;
  if (div == NODE_LEFT) {
    curr = node->left;
  } else if (div == NODE_RIGHT) {
    curr = node->right;
  }

  if (curr) {
    node->left = curr->left;
    node->right = curr->right;
  } else {
    node->left = nullptr;
    node->right = nullptr;
    node->high = 1;

    return div;
  }

  auto sub_div = recursiveDownNode(curr, select);

  if (sub_div == NODE_LEFT) {
    node->left = curr;
    if (node->right) node->right->prev = node;

  } else if (sub_div == NODE_RIGHT) {
    node->right = curr;
    if (node->left) node->left->prev = node;
  }

  node->restore();
  return div;
}

bool HeapNode::insertNode(HeapNode *curr, HeapNode *node) {
#ifdef DEBUG
  if (curr == nullptr) {
    LOG(ERROR) << "Insert heap node to null node.";
    std::exit(EXIT_FAILURE);
  }

  CHECK_HEAPNODE(node);
  CHECK_HEAPNODE(curr);
#endif

  node->prev = curr->prev;
  if (curr->prev) {
    if (curr->prev->left == curr) {
      //
      curr->prev->left = node;
    } else if (curr->prev->right == curr) {
      //
      curr->prev->right = node;
    } else {
      LOG(ERROR) << "An error node link occur.";
    }
  }

  node->left = curr->left;
  node->right = curr->right;

  auto div = recursiveDownNode(curr, [](HeapNode *l, HeapNode *r) {
    if (l == nullptr) return NODE_LEFT;
    if (r == nullptr) return NODE_RIGHT;
    if (l->high <= r->high) {
      return NODE_LEFT;
    } else {
      return NODE_RIGHT;
    }
  });

  if (div == NODE_LEFT) {
    node->left = curr;
  } else if (div == NODE_RIGHT) {
    node->right = curr;
  }

  curr->prev = node;
  node->restore();
  return true;
}

bool HeapNode::appendNode(HeapNode *curr, HeapNode *node) {
  if (curr == nullptr || curr->empty()) return false;
  if (curr->left == nullptr) {
    curr->left = node;
    node->left = node->right = nullptr;
    node->prev = curr;
    node->high = 1;

    while (curr) {
      curr->restoreHigh();
      curr = curr->prev;
    }

    return true;
  } else if (curr->right == nullptr) {
    curr->right = node;
    node->left = node->right = nullptr;
    node->prev = curr;
    node->high = 1;

    while (curr) {
      curr->restoreHigh();
      curr = curr->prev;
    }

    return true;
  } else {
    // 无法插入
    return false;
  }
}

bool HeapNode::removeNode(HeapNode *node, const DivSelect &select) {
  if (node == nullptr || node->empty()) return false;
  auto curr = node->prev;

#ifdef DEBUG
  CHECK_HEAPNODE(node);
  CHECK_HEAPNODE(curr);
#endif

  int ret = recursiveUpNode(node, select);
  if (ret == -1) return false;

  // 更新前面的high
  while (curr) {
    curr->restoreHigh();
    curr = curr->prev;
  }
  // 移除后自卷
  node->reset();
  return true;
}