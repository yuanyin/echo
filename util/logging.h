#ifndef LOGGING_H_
#define LOGGING_H_

#include <iostream>
#include <sstream>

class LogStream : public std::basic_ostringstream<char> {
 public:
  LogStream() {}
  ~LogStream() {}
};

enum class LogLevel { ALL = 0, TRACE, DEBUG, INFO, WARN, ERROR, FATAL, OFF };

class Logger {
 public:
  Logger(LogLevel level, const char* file_name, int line);
  ~Logger();

  LogStream& stream() { return stream_; }
  void formatTime();

 private:
  LogLevel level_;
  LogStream stream_;
  std::string file_name_;
  int line_;
};

std::string logLevelToStr(LogLevel level);
LogLevel minLogLevelFromEnv();
bool logTimeFromEnv();

#define LOG_TRACE Logger(LogLevel::TRACE, __FILE__, __LINE__).stream()
#define LOG_DEBUG Logger(LogLevel::DEBUG, __FILE__, __LINE__).stream()
#define LOG_INFO Logger(LogLevel::INFO, __FILE__, __LINE__).stream()
#define LOG_WARN Logger(LogLevel::WARN, __FILE__, __LINE__).stream()
#define LOG_ERROR Logger(LogLevel::ERROR, __FILE__, __LINE__).stream()

#define _LOG(level) Logger(LogLevel::level, __FILE__, __LINE__).stream()
#define _LOG_RANK(LOG_LEVEL_, rank) _LOG(LOG_LEVEL_) << "[" << rank << "]: "
#define GET_LOG(_1, _2, LOG_NAME) LOG_NAME
#define LOG(...) GET_LOG(__VA_ARGS__, _LOG_RANK, _LOG)(__VA_ARGS__)

#endif