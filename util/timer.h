// Author: Yuanyin Zhang
// Email: isyuanyin@gmail.com

#ifndef TIMER_H_
#define TIMER_H_

#include <sys/epoll.h>
#include <sys/timerfd.h>
#include <unistd.h>

#include <algorithm>
#include <chrono>
#include <ctime>
#include <functional>
#include <future>
#include <iostream>
#include <memory>
#include <mutex>

#include "util/contain.h"

#define CLOCK_ID CLOCK_MONOTONIC

struct TimeType {
 public:
  static TimeType nowTime();
  static TimeType absTime(TimeType rel);  // 相对时间 -> 绝对时间
  static TimeType relTime(TimeType rel);  // 绝对时间 -> 相对时间
  static long toSecond(long hour, long minute, long second) {
    return second + minute * 60 + hour * 60 * 60;
  }
  static const long NANO;

 public:
  TimeType(long second, long nanosecond) { reset(second, nanosecond); }
  TimeType(long second) : TimeType(second, 0) {}
  TimeType(int second) : TimeType((long)second) {}
  TimeType(unsigned int second) : TimeType((long)second) {}
  TimeType(double second)
      : TimeType((long)second, (long)(second - (long)second) * NANO) {}
  TimeType(const timespec &ts) : TimeType(ts.tv_sec, ts.tv_nsec) {}
  TimeType() : TimeType(0, 0) {}
  TimeType(const TimeType &t) { reset(t.second(), t.nanosecond()); }

  void reset(long second = 0, long nanosecond = 0) {
    time_.tv_sec = second + nanosecond / NANO;
    time_.tv_nsec = nanosecond % NANO;
  }
  long second() const { return time_.tv_sec; }
  long nanosecond() const { return time_.tv_nsec; }

  //  TimeType(std::initializer_list<long> list) {}
  TimeType &operator=(const TimeType &other) {
    reset(other.second(), other.nanosecond());
    return *this;
  }

  TimeType &operator=(int second) {
    reset(second, 0);
    return *this;
  }

  TimeType operator+(const TimeType &t) {
    return TimeType(second() + t.second(), nanosecond() + t.nanosecond());
  }
  TimeType operator+(int second) { return operator+(TimeType(second)); }

  TimeType operator-(const TimeType &t) {
    long sec = second() - t.second();
    long nsec = nanosecond() - t.nanosecond();
    return TimeType(sec, nsec);
  }

  TimeType &operator+=(const TimeType &t) {
    reset(second() + t.second(), nanosecond() + t.nanosecond());
    return *this;
  }

  // 这些运算符重载需要加上const
  bool operator<(const TimeType &t) const {
    return second() < t.second() ||
           (second() == t.second() && nanosecond() < t.nanosecond());
  }
  bool operator>(const TimeType &t) const {
    return second() > t.second() ||
           (second() == t.second() && nanosecond() > t.nanosecond());
  }
  bool operator==(const TimeType &t) const {
    return second() == t.second() && nanosecond() == t.nanosecond();
  }
  bool operator<=(const TimeType &other) const {
    return operator<(other) || operator==(other);
  }
  bool operator>=(const TimeType &other) const {
    return operator>(other) || operator==(other);
  }

  operator long() const { return second(); }
  operator int() const { return second(); }
  operator unsigned int() const { return second(); }
  operator double() const { return second() + nanosecond() * 1.0 / NANO; }
  operator timespec() { return time_; }

 private:
  timespec time_;
};

class TimeEvent {
 public:
  static void asyncThread();
  static void asyncExecute();

  static void startTimeEvent(int epfd, const TimeType &value = TIME_SLOT,
                             const TimeType &interval = TIME_SLOT);
  static void stopTimeEvent() { setTimeout(0, 0); }
  static void handleTimeEvent();
  static bool isTimeEvent(int fd) { return kTimerFd == fd; }

  static void setTimeout(const TimeType &value, const TimeType &interval = 0);
  static void setInternal(const TimeType &interval);

 public:
  static TimeType TIME_SLOT;

 private:
  static int kTimerFd;
};

#define CHECK_HEAPNODE(this)                                       \
  do {                                                             \
    if (!this) break;                                              \
    if (this->prev) {                                              \
      if (this->prev->right != this && this->prev->left != this) { \
        LOG(ERROR) << "Error node prev link\n";                    \
        std::exit(EXIT_FAILURE);                                   \
      }                                                            \
    }                                                              \
    if (this->left && this->left->prev != this) {                  \
      LOG(ERROR) << "Error node left link\n";                      \
      std::exit(EXIT_FAILURE);                                     \
    }                                                              \
    if (this->right && this->right->prev != this) {                \
      LOG(ERROR) << "Error node right link\n";                     \
      std::exit(EXIT_FAILURE);                                     \
    }                                                              \
  } while (false)

typedef std::function<void()> Action;

struct TimerNode {
  TimeType expire;
  Action action;

  TimeType getExpiredTime() { return expire; }
  void setExpriedTime(TimeType exp) { expire = exp; }

  TimerNode() = delete;
  TimerNode(TimeType exp, Action act = nullptr) : expire(exp), action(act) {}
  virtual ~TimerNode() {}
};

class TimerPool {
 public:
#ifdef DEBUG
  static TimerPool *GetPool(TimerPool *pool = nullptr);
#else
  static TimerPool *GetPool();
#endif

 public:
  virtual bool addTimer(TimerNode *) = 0;
  virtual bool delTimer(TimerNode *) = 0;
  virtual bool updateTimer(TimerNode *timer_node, TimeType expire) {
    if (delTimer(timer_node)) {
      timer_node->setExpriedTime(expire);
      return addTimer(timer_node);
    }
    return false;
  }
  virtual TimerNode *topTimer() = 0;
  virtual bool popTimer() { return delTimer(topTimer()); }

  virtual bool isInPool(TimerNode *) = 0;

  virtual void clear() = 0;
  virtual int size() const = 0;
  virtual bool empty() const { return size() == 0; }

  virtual std::unique_ptr<TimerNode> generateNode(TimeType exp, Action cb) = 0;

 protected:
  TimerPool() = default;
  virtual ~TimerPool();
};

class TimerList : public TimerPool {
 public:
  TimerList() : TimerPool(), list_head_(new ListNode()), size_(0) {}
  ~TimerList() { delete list_head_; }

 public:
  struct TimerListNode : public TimerNode, public ListNode {
   public:
    static TimeType nodeToExpiredTime(ListNode *node);
    static int compareExpiredTime(ListNode *n1, ListNode *n2);

   public:
    TimerListNode(TimeType exp, Action cb = nullptr);
  };

 public:
  std::unique_ptr<TimerNode> generateNode(TimeType exp, Action cb) override {
    return std::make_unique<TimerListNode>(exp, cb);
  }

  bool addTimer(TimerNode *timer_node) override;
  bool delTimer(TimerNode *timer_node) override;
  TimerNode *topTimer() override;

  bool isInPool(TimerNode *timer_node) override;
  void clear() override;
  int size() const override { return size_; }

 private:
  ListNode *list_head_;
  int size_;
  mutable std::mutex mutex_;
};

class TimerHeap : public TimerPool {
 public:
  TimerHeap() : heap_top_(nullptr), size_(0) {}
  ~TimerHeap() {}

 public:
  struct TimerHeapNode : public TimerNode, public HeapNode {
   public:
    TimerHeapNode(TimeType exp, Action action = nullptr)
        : TimerNode(exp, action) {}

   public:
    static TimeType nodeToExpiredTime(HeapNode *node);
    static int compareExpiredTime(HeapNode *n1, HeapNode *n2);
  };

 public:
  std::unique_ptr<TimerNode> generateNode(TimeType exp, Action cb) override;

  bool addTimer(TimerNode *timer_node) override;
  bool delTimer(TimerNode *timer_node) override;
  bool updateTimer(TimerNode *timer_node, TimeType expire) override;
  TimerNode *topTimer() override;

  bool isInPool(TimerNode *timer_node) override;
  void clear() override;
  int size() const override { return size_; }

 private:
  HeapNode *heap_top_;
  int size_;
  mutable std::mutex mutex_;
};

class TimerImpl {
 public:
  static void tick();
  static int size() { return TimerPool::GetPool()->size(); }
  static void clear() { TimerPool::GetPool()->clear(); }

 public:
  TimerImpl(TimeType exp, Action action);
  ~TimerImpl() { this->stop(); }

  void setAction(Action action) { node_->action = action; }

  void start() { TimerPool::GetPool()->addTimer(node_.get()); }
  void stop() { TimerPool::GetPool()->delTimer(node_.get()); }
  void update(TimeType expire) {
    auto pool = TimerPool::GetPool();
    pool->updateTimer(node_.get(), expire);
  }
  TimeType expire() { return node_->getExpiredTime(); }

  bool empty() { return !TimerPool::GetPool()->isInPool(node_.get()); }

 private:
  std::unique_ptr<TimerNode> node_;
};

class Timer {
 public:
  static void tick() { TimerImpl::tick(); }
  static int size() { return TimerImpl::size(); }
  static void clear() { TimerImpl::clear(); }

 public:
  void start() { timer_impl_->start(); }
  void stop() { timer_impl_->stop(); }
  void update(TimeType expire) { timer_impl_->update(expire); }
  TimeType expire() { return timer_impl_->expire(); }
  bool empty() { return timer_impl_->empty(); }

  template <typename F, typename... Args>
  auto setTimeout(TimeType timeout, F &&func, Args &&... args)
      -> std::future<typename std::result_of<F(Args...)>::type>;

  template <typename F, typename... Args>
  void setInterval(TimeType timeout, F &&func, Args &&... args);

 private:
  void set_interval(TimeType timeout, Action act) {
    Action action = [this, timeout, act] {
      act();
      this->set_interval(timeout, act);
    };
  }

 public:
  Timer() : Timer(0) {}
  Timer(TimeType exp, Action action = nullptr, bool start = false);
  Timer(Timer &&t) : timer_impl_(std::move(t.timer_impl_)) {}
  ~Timer() {}

 private:
  std::unique_ptr<TimerImpl> timer_impl_;
};

template <typename F, typename... Args>
auto Timer::setTimeout(TimeType timeout, F &&func, Args &&... args)
    -> std::future<typename std::result_of<F(Args...)>::type> {
  using return_type = typename std::result_of<F(Args...)>::type;

  auto task_ptr = std::make_shared<std::packaged_task<return_type()>>(
      std::bind(std::forward<F>(func), std::forward<Args>(args)...));

  std::future<return_type> res = task_ptr->get_future();

  Action action = [task_ptr] { (*task_ptr)(); };

  return res;
}

template <typename F, typename... Args>
void Timer::setInterval(TimeType timeout, F &&func, Args &&... args) {
  std::bind(std::forward<F>(func), std::forward<Args>(args)...);
}

#endif