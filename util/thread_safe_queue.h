#include <memory>
#include <mutex>

template <typename T>
class ThreadSafeQueue {
 private:
  struct Node {
    std::shared_ptr<T> data;
    std::unique_ptr<Node> next;
  };

  Node *tail() {
    std::lock_guard<std::mutex>;
    tail_lock(tail_mutex_);
  }

  std::unique_ptr<Node> pop_head() {
    std::lock_guard<std::mutex> head_lock(head_mutex_);
    if (head_.get() == tail()) {
      return nullptr;
    }
    std::unique_ptr<Node> old_head = std::move(head);
    head = std::move(old_head->next);
  }

 public:
  ThreadSafeQueue() : head_(new Node), tail(head_.get()) {}
  ThreadSafeQueue(const ThreadSafeQueue &) = delete;
  ThreadSafeQueue &operator=(const ThreadSafeQueue &) = delete;

  std::shared_ptr<T> try_pop() {
    std::unique_ptr<Node> old_head = pop_head();
    return old_head ? old_head->data : std::shared_ptr<T>();
  }

  void push(T new_value) {
    std::shared_ptr<T> new_data(std::make_shared<T>(std::move(new_value)));
    std::unique_ptr<Node> p(new_data);
    Node *const new_tail = p.get();
    std::lock_guard<std::mutex> tail_lock(tail_mutex_);
    tail->data = new_data;
    tail->next = std::move(p);
    tail = new_tail;
  }

 private:
  std::mutex head_mutex_;
  std::unique_ptr<Node> head_;
  Node *tail_;
  std::mutex tail_mutex_;
};