#include "logging.h"

#include <algorithm>
#include <chrono>
#include <ctime>
#include <iomanip>

/// Logger Implemetation
///
Logger::Logger(LogLevel level, const char* file_name, int line)
    : level_(level), file_name_(file_name), line_(line), stream_() {
  static bool log_time = logTimeFromEnv();

  std::string log_level_str = logLevelToStr(level_);

  if (log_time) {
    auto now = std::chrono::system_clock::now();
    auto as_time_t = std::chrono::system_clock::to_time_t(now);

    auto duration = now.time_since_epoch();
    auto seconds = std::chrono::duration_cast<std::chrono::seconds>(duration);
    auto micros_remainder =
        std::chrono::duration_cast<std::chrono::microseconds>(duration -
                                                              seconds);

    const size_t time_buffer_size = 30;
    char time_buffer[time_buffer_size];
    strftime(time_buffer, time_buffer_size, "%Y-%m-%d %H:%M:%S",
             localtime(&as_time_t));
    stream_ << "[" << time_buffer << " " << log_level_str << " " << file_name
            << ":" << line_ << "] ";
  } else {
    stream_ << "[" << log_level_str << " " << file_name_ << ":" << line_
            << "] ";
  }
}

void Logger::formatTime() {
  char time_str[30] = {0};
  timespec ts;
  clock_gettime(CLOCK_REALTIME, &ts);
  tm* date_time = ::localtime(&ts.tv_sec);
  strftime(time_str, 26, "%Y-%m-%d %H:%M:%S", date_time);
  stream_ << time_str;
}

Logger::~Logger() {
  static LogLevel min_log_level = minLogLevelFromEnv();

  if (level_ >= min_log_level) {
    bool use_cout =
        static_cast<int>(level_) <= static_cast<int>(LogLevel::INFO);
    std::ostream& os = use_cout ? std::cout : std::cerr;

    stream_ << std::endl;
    os << stream_.str();
  }

  if (level_ == LogLevel::FATAL) {
    std::abort();
  }
}

std::string logLevelToStr(LogLevel level) {
  switch (level) {
    case LogLevel::ALL:
      return "ALL";
    case LogLevel::TRACE:
      return "TRACE";
    case LogLevel::DEBUG:
      return "DEBUG";
    case LogLevel::INFO:
      return "INFO";
    case LogLevel::WARN:
      return "WARN";
    case LogLevel::ERROR:
      return "ERROR";
    case LogLevel::FATAL:
      return "FATAL";
    default:
      return "WARN";
  }
}

/// Helper Function
LogLevel parseLogLevelStr(const char* raw_str) {
  std::string log_level_str(raw_str);
  std::transform(log_level_str.begin(), log_level_str.end(),
                 log_level_str.begin(), ::tolower);

  if (log_level_str == "strace") {
    return LogLevel::TRACE;
  } else if (log_level_str == "debug") {
    return LogLevel::DEBUG;
  } else if (log_level_str == "info") {
    return LogLevel::INFO;
  } else if (log_level_str == "warn") {
    return LogLevel::WARN;
  } else if (log_level_str == "error") {
    return LogLevel::ERROR;
  } else if (log_level_str == "fatal") {
    return LogLevel::FATAL;
  } else {
    return LogLevel::WARN;
  }
}

LogLevel minLogLevelFromEnv() {
  const char* env_var_val = getenv("ECHO_LOG_LEVEL");
  if (env_var_val == nullptr) {
    return LogLevel::TRACE;
  }
  return parseLogLevelStr(env_var_val);
}

bool logTimeFromEnv() {
  const char* env_var_val = getenv("ECHO_LOG_HIDE_TIME");
  if (env_var_val != nullptr && std::strtol(env_var_val, nullptr, 10) > 0) {
    return false;
  } else {
    return true;
  }
}