#ifndef CONTAIN_H_
#define CONTAIN_H_

#include <functional>
#include <memory>
#include <unordered_map>

struct ListNode {
  ListNode *prev, *next;
  ListNode() : prev(this), next(this) {}
  ListNode(ListNode *p, ListNode *n) : prev(p), next(n) {}
  virtual ~ListNode() {}
  static bool insertNode(ListNode *curr, ListNode *node);
  static bool removeNode(ListNode *node);
};

struct HeapNode {
  enum NODE_DIV { NODE_LEFT = 0, NODE_RIGHT, NODE_NONE };
  typedef std::function<NODE_DIV(HeapNode *l, HeapNode *r)> DivSelect;

  HeapNode *prev, *left, *right;
  int high;

#ifdef DEBUG
  int id;
#endif

  // 通过"自卷"表示是在Pool中
  HeapNode() : prev(this), left(this), right(this) {}
  HeapNode(HeapNode *p, HeapNode *l = nullptr, HeapNode *r = nullptr)
      : prev(p), left(l), right(r) {}
  virtual ~HeapNode() {}

  void reset() {
    prev = left = right = this;
    high = 0;
  }

  bool empty() { return prev == this || left == this || right == this; }

  void restore() {
    restoreLink();
    restoreHigh();
  }
  void restoreLink() {
    if (this->left) {
      this->left->prev = this;
    }
    if (this->right) {
      this->right->prev = this;
    }
  }

  void restoreHigh() {
    high = std::max(left ? left->high : 0, right ? right->high : 0) + 1;
  }

 public:
  static bool upNode(HeapNode *node, NODE_DIV div = NODE_NONE);
  static bool downNode(HeapNode *node, NODE_DIV div);

  static int recursiveUpNode(HeapNode *node, const DivSelect &select);
  static NODE_DIV recursiveDownNode(HeapNode *node, const DivSelect &select);

  // 插入操作 保持平衡
  static bool insertNode(HeapNode *curr, HeapNode *node);
  static bool appendNode(HeapNode *curr, HeapNode *node);

  // 删除操作 保证结构 (注意空值情况)
  static bool removeNode(HeapNode *node, const DivSelect &select);
};

struct LruCache {
 public:
  struct LruNode : ListNode {
   private:
  };
};

#endif