#ifndef THREAD_POOL_H_
#define THREAD_POOL_H_

#include <condition_variable>
#include <functional>
#include <future>
#include <memory>
#include <mutex>
#include <queue>
#include <stdexcept>
#include <thread>
#include <vector>

class ThreadPoolBase {
 public:
  virtual std::size_t size() const = 0;

  // The number of idle thread.
  virtual std::size_t numAvailable() const = 0;

  // Check if the current thread is from the thread pool.
  virtual bool inThreadPool() const = 0;

  virtual ~ThreadPoolBase() noexcept {}

  static std::size_t defaultNumThreads() {
    auto num_threads = std::thread::hardware_concurrency();
    return num_threads;
  }
};

class ThreadPool : public ThreadPoolBase {
 public:
  ThreadPool() = delete;

  explicit ThreadPool(std::size_t pool_size,
                      std::function<void()> init_thread = nullptr);

  ~ThreadPool();

  std::size_t size() const override;

  std::size_t numAvailable() const override;

  bool inThreadPool() const override;

  template <class F, class... Args>
  auto runAny(F&& f, Args&&... args)
      -> std::future<typename std::result_of<F(Args...)>::type>;

  void run(std::function<void()> func) {
    {
      std::unique_lock<std::mutex> lock(mutex_);
      if (stop_) {
        throw std::runtime_error("Enqueue a task in a stopped ThreadPool");
      }
      tasks_.emplace(func);
      complete_ = false;
    }

    condition_.notify_one();
  }

  /// @brief Wait for queue to be empty
  void waitAllComplete();

 protected:
  std::vector<std::thread> threads_;
  std::queue<std::function<void()>> tasks_;

  mutable std::mutex mutex_;
  std::condition_variable condition_;
  std::condition_variable completed_;
  bool stop_;
  bool complete_;
  std::size_t total_;
  std::size_t available_;  // 空闲的线程个数（没有在执行任务）

 private:
  void main_loop();
};

template <typename F, typename... Args>
auto ThreadPool::runAny(F&& f, Args&&... args)
    -> std::future<typename std::result_of<F(Args...)>::type> {
  if (threads_.size() == 0) {
    throw std::runtime_error("No threads to run a task");
  }

  // std::result_of<>::type是函数的返回值类型
  using return_type = typename std::result_of<F(Args...)>::type;

  auto task_ptr = std::make_shared<std::packaged_task<return_type()>>(
      std::bind(std::forward<F>(f), std::forward<Args>(args)...));

  // 分离返回值和函数体
  std::future<return_type> res = task_ptr->get_future();
  {
    std::unique_lock<std::mutex> lock(mutex_);
    if (stop_) {
      throw std::runtime_error("Enqueue a task in a stopped ThreadPool");
    }
    // In here task_ptr is the point to a std::packaged_task<return_type()>
    // object and (*task_ptr)() == f(args)，
    tasks_.emplace([task_ptr]() { (*task_ptr)(); });
    complete_ = false;
  }
  // 通知一个消费者线程，有新任务了
  condition_.notify_one();
  return res;
}

#endif