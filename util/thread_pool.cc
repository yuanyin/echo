#include "thread_pool.h"

/**
 * @brief Construct a new ThreadPool object
 *
 * @param pool_size specify the size of thread pool
 * @param init_thread initial function before main loop
 */
ThreadPool::ThreadPool(std::size_t pool_size, std::function<void()> init_thread)
    : threads_(pool_size < 0 ? defaultNumThreads() : pool_size),
      stop_(false),
      complete_(true),
      total_(threads_.size()),
      available_(threads_.size()) {
  for (std::size_t i = 0; i < threads_.size(); ++i)
    threads_[i] = std::thread([this, init_thread] {
      if (init_thread) {
        init_thread();
      }
      this->main_loop();
    });
}

ThreadPool::~ThreadPool() {
  // 终止所有子线程
  {
    std::unique_lock<std::mutex> lock(mutex_);
    stop_ = true;
    condition_.notify_all();
  }

  for (auto& t : threads_) {
    try {
      t.join();
    } catch (const std::exception&) {
    }
  }
}

size_t ThreadPool::size() const { return threads_.size(); }

size_t ThreadPool::numAvailable() const {
  std::unique_lock<std::mutex> lock(mutex_);
  return available_;
}

bool ThreadPool::inThreadPool() const {
  for (auto& thread : threads_) {
    if (thread.get_id() == std::this_thread::get_id()) {
      return true;
    }
  }
  return false;
}

void ThreadPool::waitAllComplete() {
  std::unique_lock<std::mutex> lock(mutex_);
  while (!complete_) {
    completed_.wait(lock);
  }
}

void ThreadPool::main_loop() {
  std::unique_lock<std::mutex> lock(this->mutex_);
  while (true) {
    // [条件变量]等待主线程设置stop或者任务队列不空
    this->condition_.wait(
        lock, [this] { return this->stop_ || !this->tasks_.empty(); });

    // 主线程设置停止执行，同时任务队列为空
    if (this->stop_ && this->tasks_.empty()) {
      return;
    }

    {
      // 将队首的任务拿出来执行
      std::function<void()> task = std::move(this->tasks_.front());
      this->tasks_.pop();
      --available_;

      lock.unlock();

      try {
        task();  // 执行任务
      } catch (const std::exception& e) {
        // LOG(ERROR) << "Exception in thread pool task: " << e.what();
      } catch (...) {
        // LOG(ERROR) << "Exception in thread pool task: unknown";
      }
    }

    lock.lock();

    ++available_;
    if (tasks_.empty() && available_ == total_) {
      complete_ = true;
      completed_.notify_one();
    }
  }
}