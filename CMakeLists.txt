cmake_minimum_required(VERSION 3.13)
project(echo VERSION 0.1)

set(CMAKE_CXX_STANDARD 17)

file(GLOB RESOURCES_FILES  "${PROJECT_SOURCE_DIR}/static")

include_directories(
  "."
  "${CMAKE_SOURCE_DIR}/include"
)

add_library(echo "")
target_sources(echo
PRIVATE
  "echo/echo.cc"
  "echo/context.h"
  "echo/context.cc"

)

add_subdirectory(util)
add_subdirectory(mime)
add_subdirectory(io)
add_subdirectory(net)

find_package(Threads REQUIRED)
target_link_libraries(echo PUBLIC Threads::Threads echo_io echo_util echo_net echo_mime)

add_executable(echox echox.cc)
target_link_libraries(echox echo)